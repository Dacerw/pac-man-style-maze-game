import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;

/**
 * This class is responsible for drawing the score screen after the game has ended.
 * It is also responsible for adding a mouse listener for this class to the game's window.
 */
public class DrawEndScreen implements DrawState {
	private static final String endPath = new String("resources/end/");
	private static final String spritesAssetsPath = new String("resources/sprite assets/");
	
	private int wWidth;
	private int wHeight;
	
	public Game game;
	private DrawEndScreenInput endMl;
	
	private RectangleComponent continueButton;
	
	public BufferedImage endState;
	public BufferedImage endSingle;
	public BufferedImage endSingleClick;
	public BufferedImage endMulti;
	public BufferedImage endMultiClick;
	
	private BufferedImage firstPlayer;
	private BufferedImage secondPlayer;
	
	int numPlayers;
	DrawEndScreen(Game game){
		this.game = game;
		wWidth = game.getWindowWidth();
		wHeight = game.getWindowHeight();
		
		loadEndAssets();
		loadSpritesAssets();
		numPlayers = game.getGameOptions().getNumPlayers();
		if (numPlayers == 1){
			endState = endSingle;
		} else {
			endState = endMulti;
		}
		
		continueButton = new RectangleComponent(wWidth*59/98, wHeight*44/81, wWidth*10/55, wHeight*9/66);
		
		endMl = new DrawEndScreenInput(this, continueButton, numPlayers);
		game.getWindow().addMouseListener(endMl);
		game.getWindow().addMouseMotionListener(endMl);
	}

	/**
	 * Displays graphics for the DrawEndScreen
	 */
	@Override
	public void render() {
		Window window = game.getWindow();	
		BufferStrategy bs = window.getBufferStrategy();
		
		wWidth = window.getWidth();
		wHeight = window.getHeight();
		
		//Updates the location/size of the buttons
		continueButton.updateLocation(wWidth*59/98, wHeight*44/81);
		continueButton.updateSize(wWidth*10/55, wHeight*9/66);
		
		//Draws a rectangle grey background
		Graphics g = bs.getDrawGraphics();
		g.setColor(Color.gray);
		g.fillRect(0,0,wWidth,wHeight);
		
		Graphics2D g2 = (Graphics2D)g;
	    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		
	    //Selects the font/colour that will be used when drawing strings
	    int fontSize = (wHeight+wWidth)/55;
        Font myFont = new Font("Arial", Font.BOLD, fontSize);
        g2.setFont(myFont);
        g2.setColor(Color.BLACK);
		
        //Renders a single player end-game screen
		if (numPlayers == 1) {
			g2.drawImage(endState, wWidth*1/5, wHeight*3/10, wWidth*3/5, wHeight*2/5, null);
			g2.drawImage(firstPlayer, wWidth*16/25, wHeight*7/20, wWidth/12, wHeight/6, null);
			
			//Draws a win/lose string, depending on whether or not the player has won
			if (game.gameLogic.hasPlayerOneWon == 1){
				g2.drawString(("Congratulations! You won!"),wWidth*9/40, wHeight*9/20);
				g2.drawString("Final Score:   " + Integer.toString(game.getPlayer(1).getScore()), wWidth*9/40, wHeight*23/40);
			} else {
				g2.drawString(("You lose. Try again!"),wWidth*12/40, wHeight*1/2);
			}
		
		//Renders a multiplayer end-game screen
		} else {
		   g2.drawImage(endState, wWidth*1/5, wHeight*3/10, wWidth*3/5, wHeight*2/5, null);
		   g2.drawImage(firstPlayer, wWidth*15/25, wHeight*7/20, wWidth/12, wHeight/6, null);
           g2.drawImage(secondPlayer, wWidth*35/50, wHeight*7/20, wWidth/12, wHeight/6, null);
          
           //Draws a win/lose string, depending on which player has won (or if players drew)
           if (game.gameLogic.hasPlayerOneWon == 1){
				g2.drawString(("Congratulations Player 1!"),wWidth*9/40, wHeight*9/20);
				g2.drawString("P1 Final Score:   " + Integer.toString(game.getPlayer(1).getScore()), wWidth*9/40, wHeight*23/40);
				g2.drawString("P2 Final Score:   " + Integer.toString(game.getPlayer(2).getScore()), wWidth*9/40, wHeight*26/40);
			} else if (game.gameLogic.hasPlayerOneWon == 0){
				g2.drawString(("Draw!"),wWidth*16/40, wHeight*9/20);
				g2.drawString("P1 Final Score:   " + Integer.toString(game.getPlayer(1).getScore()), wWidth*9/40, wHeight*23/40);
				g2.drawString("P2 Final Score:   " + Integer.toString(game.getPlayer(2).getScore()), wWidth*9/40, wHeight*26/40);
			} else {
				g2.drawString(("Congratulations Player 2!"),wWidth*9/40, wHeight*9/20);
				g2.drawString("P1 Final Score:   " + Integer.toString(game.getPlayer(1).getScore()), wWidth*9/40, wHeight*23/40);
				g2.drawString("P2 Final Score:   " + Integer.toString(game.getPlayer(2).getScore()), wWidth*9/40, wHeight*26/40);
			}
		}
		g.dispose();
		bs.show();
	}
	
	/**
	 * Destroys the DrawEndScreen's mouse listener resources
	 */
	@Override
	public void dispose() {
		game.getWindow().removeMouseListener(endMl);
		game.getWindow().removeMouseMotionListener(endMl);
	}

	/**
	 * Loads sprite assets from its file path
	 */
	private void loadSpritesAssets() {
		firstPlayer = GraphicsUtilities.loadImage(spritesAssetsPath + "spr1_d0.png");
		secondPlayer = GraphicsUtilities.loadImage(spritesAssetsPath + "spr2_d0.png");
	}
	
	/**
	 * Loads end screen assets from its file path
	 */
	private void loadEndAssets(){
		endSingle = GraphicsUtilities.loadImage(endPath + "EndSingle.jpg");
		endSingleClick = GraphicsUtilities.loadImage(endPath + "EndSingleClick.jpg");
		endMulti = GraphicsUtilities.loadImage(endPath + "EndMulti.jpg");
		endMultiClick = GraphicsUtilities.loadImage(endPath + "EndMultiClick.jpg");
	}
}
