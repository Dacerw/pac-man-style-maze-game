
/**
 * A class used to represent a point in a 2d cartesian graph, where
 * coordinates can be held in integers.
 */
public class Point {
	
	//Integers containing the coordinates of the point
	private int x;
	private int y;
	
	/**
	 * Creates a Point object given its x and y coordinates.
	 * @param x an integer containing the x coordinate of the point.
	 * @param y an integer containing the y coordinate of the point.
	 */
	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Gets the x-coordinate of the point.
	 * @return an integer containing the x-coordinate.
	 */
	public int getX() {
		return this.x;
	}
	
	/**
	 * Gets the y-coordinate of the point.
	 * @return an integer containing the y-coordinate.
	 */
	public int getY() {
		return this.y;
	}
	
	/**
	 * Overrides the Object method.
	 */
	@Override
	public boolean equals(Object o) {
		if (o == null) return false;
		if (o == this) return true;
		if (!(o instanceof Point)) return false;
		Point s = (Point) o;
		return (s.getX() == this.getX() && s.getY() == this.getY());
	}
	
	/**
	 * Overrides the Object method
	 */
	@Override
	public int hashCode() {
		int hash = 37 + x;
		hash = hash*37 + y;
		return hash;
	}
}
