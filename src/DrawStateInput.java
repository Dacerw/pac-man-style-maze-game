import java.awt.Component;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

/**
 * Used by graphics screens to receive and manage input from them.
 */
public abstract class DrawStateInput implements MouseListener, MouseMotionListener {
	/**
	 * Checks if a mouse is over a specified component 
	 * @param c A component to which the mouse will be checked.
	 * @param mouseX an integer containing the x location of the mouse.
	 * @param mouseY an integer containing the y location of the mouse.
	 * @return a boolean value containing true if the mouse is over the component, and false
	 * otherwise.
	 */
	public boolean mouseOver(Component c, int mouseX, int mouseY) {
			return (c.getLocation().x <= mouseX && c.getLocation().x + c.getWidth() >= mouseX &&
				c.getLocation().y <= mouseY && c.getLocation().y + c.getHeight() >= mouseY);
	}
}
