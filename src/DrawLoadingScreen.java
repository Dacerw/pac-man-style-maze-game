import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;

/**
 * This class is responsible for drawing the loading screen after the game options have been picked.
 * It is also responsible for adding a mouse listener for this class to the game's window.
 */
public class DrawLoadingScreen implements DrawState {
	private static final String loadingScreenPath = "resources/loading/";
	
	private int wWidth;
	private int wHeight;
	
	public Game game;
	public DrawMaze drawMaze;
	private DrawLoadingScreenInput loadingML;
	
	private RectangleComponent continueRect;
	
	public BufferedImage curImage;
	public BufferedImage loadingReady;
	public BufferedImage loadingReadyClick;
	public BufferedImage loadingWaiting;
	
	public DrawLoadingScreen(Game game, DrawMaze drawMaze) {
		this.game = game;
		this.drawMaze = drawMaze;
		wWidth = game.getWindowWidth();
		wHeight = game.getWindowHeight();
		
		loadAssets();
		curImage = loadingWaiting;
		
		continueRect = new RectangleComponent(wWidth*100/137, wHeight*58/77, wWidth*47/274, wHeight*9/77);
		
		loadingML = new DrawLoadingScreenInput(this, continueRect);
		game.getWindow().addMouseListener(loadingML);
		game.getWindow().addMouseMotionListener(loadingML);
		
	}

	/**
	 * Displays graphics for the DrawLoadingScreen
	 */
	@Override
	public void render() {
		Window window = game.getWindow();
		BufferStrategy bs = window.getBufferStrategy();
		Graphics g = bs.getDrawGraphics();
		g.fillRect(0,0,wWidth,wHeight);
		
		wWidth = window.getWidth();
		wHeight = window.getHeight();
		
		//Updates the loading status 
		if (curImage == loadingWaiting && drawMaze.isLoading == false) {
			curImage = loadingReady;
		}
		
		//Draws the appropriate DrawLoadingScreen image
		g.drawImage(curImage, 0, 0, wWidth, wHeight, null);
		
		//Updating the location/size of the button
		continueRect.updateLocation(wWidth*100/137, wHeight*58/77);
		continueRect.updateSize(wWidth*47/274, wHeight*9/77);
		
		g.dispose();
		bs.show();
	}

	/**
	 * Destroys the DrawLoadingScreen's mouse listener resources
	 */
	@Override
	public void dispose() {
		game.getWindow().removeMouseListener(loadingML);
		game.getWindow().removeMouseMotionListener(loadingML);
	}
	
	/**
	 * Loads background image assets from its file path
	 */
	private void loadAssets() {
		loadingReady = GraphicsUtilities.loadImage(loadingScreenPath+"LoadingReady.jpg");
		loadingReadyClick = GraphicsUtilities.loadImage(loadingScreenPath+"LoadingReadyClick.jpg");
		loadingWaiting = GraphicsUtilities.loadImage(loadingScreenPath+"LoadingWaiting.jpg");
	}
}
