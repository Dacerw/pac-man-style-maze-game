import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * This class is responsible for drawing the game options screen and allowing the user to change
 * the difficulty and singleplayer/multiplayer settings of the game.
 * It is also responsible for adding a mouse listener for this class to the game's window.
 */
public class DrawGameOptions implements DrawState {
	private static final String gameOptionsAssetsPath = new String("resources/game options");
	private static final String spriteAssetsPath = new String("resources/sprite assets/");
	
	private int wWidth;
	private int wHeight;
	
	public Game game;
	private DrawGameOptionsInput optionsMl;
	
	private RectangleComponent easyButton;
	private RectangleComponent mediumButton;
	private RectangleComponent hardButton;
	private RectangleComponent singleplayerButton;
	private RectangleComponent multiplayerButton;
	private RectangleComponent backButton;
	private RectangleComponent playButton;
	
	private BufferedImage firstPlayer;
	private BufferedImage secondPlayer;
	
	//2 -> SP/MP
	//3 -> Easy/Medium/Hard
	//3 -> None/Back/Play
	private BufferedImage optionState[][][] = new BufferedImage[2][3][3];
	
	public int provNumPlayers;
	public int provDifficulty;
	public int stateHover;
	
	public DrawGameOptions(Game game) {
		this.game = game;
		
		wWidth = game.getWindowWidth();
		wHeight = game.getWindowHeight();
		
		stateHover = 0;
		provNumPlayers = game.getGameOptions().getNumPlayers();
		provDifficulty = game.getGameOptions().getDifficulty();
		
		loadSpritesAssets();
		
		easyButton = new RectangleComponent(wWidth*58/273, wHeight*15/77, wWidth*47/273, wHeight*8/77);
		mediumButton = new RectangleComponent(wWidth*16/39, wHeight*15/77, wWidth*47/273, wHeight*8/77);
		hardButton = new RectangleComponent(wWidth*166/273, wHeight*15/77, wWidth*47/273, wHeight*8/77);
		singleplayerButton = new RectangleComponent(wWidth*15/34, wHeight*75/154, wWidth*3/68, wHeight*6/77);
		multiplayerButton = new RectangleComponent(wWidth*69/136, wHeight*75/154, wWidth*3/68, wHeight*6/77);
		backButton = new RectangleComponent(wWidth/68, wHeight*9/385, wWidth*9/85, wHeight/14);
		playButton = new RectangleComponent(wWidth*99/136, wHeight*58/77, wWidth*29/70, wHeight*4/35);
		
		optionsMl = new DrawGameOptionsInput(this, easyButton, mediumButton, hardButton, singleplayerButton, multiplayerButton, backButton, playButton);
		game.getWindow().addMouseListener(optionsMl);
		game.getWindow().addMouseMotionListener(optionsMl);
	}
	
	/**
	 * Displays graphics for the DrawGameOptions screen
	 */
	public void render() {
		Window window = game.getWindow();
		BufferStrategy bs = window.getBufferStrategy();
		Graphics g = bs.getDrawGraphics();	
		g.fillRect(0,0,wWidth,wHeight);
		
		wWidth = window.getWidth();
		wHeight = window.getHeight();
	
		//Updates the location/size of buttons
		easyButton.updateLocation(wWidth*58/273, wHeight*15/77);
		easyButton.updateSize(wWidth*47/273, wHeight*8/77);
		mediumButton.updateLocation(wWidth*16/39, wHeight*15/77);
		mediumButton.updateSize(wWidth*47/273, wHeight*8/77);
		hardButton.updateLocation(wWidth*166/273, wHeight*15/77);
		hardButton.updateSize(wWidth*47/273, wHeight*8/77);
		singleplayerButton.updateLocation(wWidth*15/34, wHeight*75/154);
		singleplayerButton.updateSize(wWidth*3/68, wHeight*6/77);
		multiplayerButton.updateLocation(wWidth*69/136, wHeight*75/154);
		multiplayerButton.updateSize(wWidth*3/68, wHeight*6/77);
		backButton.updateLocation(wWidth/68, wHeight*9/385);
		backButton.updateSize(wWidth*9/85, wHeight/14);
		playButton.updateLocation(wWidth*99/136, wHeight*58/77);
		playButton.updateSize(wWidth*29/70, wHeight*4/35);
		
		//Updates and draws the appropriate GameOptions image
		BufferedImage image = optionState[provNumPlayers-1][provDifficulty][stateHover];
		if (image == null) {
			image = GraphicsUtilities.loadImage(getPath(provNumPlayers, provDifficulty, stateHover));
		}
		optionState[provNumPlayers-1][provDifficulty][stateHover] = image;
		g.drawImage(image, 0, 0, wWidth, wHeight, null);
		
		if (provNumPlayers == 1) {
			g.drawImage(firstPlayer, wWidth*11/25, wHeight*13/20, wWidth/10, wHeight/5, null);
		} else if (provNumPlayers == 2) {
			g.drawImage(firstPlayer, wWidth*7/20, wHeight*13/20, wWidth/10, wHeight/5, null);
			g.drawImage(secondPlayer, wWidth*27/50, wHeight*13/20, wWidth/10, wHeight/5, null);
		}
		
		g.dispose();
		bs.show();
	}
	
	/**
	 * Destroys the DrawGameOption screen's mouse listener resources
	 */
	@Override
	public void dispose() {
	  	game.getWindow().removeMouseListener(optionsMl);
		game.getWindow().removeMouseMotionListener(optionsMl);
	}
	
	/**
	 * Loads sprite assets from its file path
	 */
	private void loadSpritesAssets() {
		firstPlayer = GraphicsUtilities.loadImage(spriteAssetsPath + "spr1_d0.png");
		secondPlayer = GraphicsUtilities.loadImage(spriteAssetsPath + "spr2_d0.png");
	}
	
	/**
	 * Loads the appropriate game options assets from its file path
	 * @param numPlayers an integer conttaining the number of players in the game.
	 * @param difficulty an integer representing the difficulty of the game.
	 * @param stateHover an integer representing what the component the user is interacting with.
	 * @return
	 */
	private String getPath(int numPlayers, int difficulty, int stateHover) {
		String p = (numPlayers == 1) ? "Single" : "Multi";
		String d = null;
		if (difficulty == GameOptions.EASY_DIFFICULTY) {
			d = new String("Easy");
		} else if (difficulty == GameOptions.MEDIUM_DIFFICULTY) {
			d = new String("Medium");
		} else if (difficulty == GameOptions.HARD_DIFFICULTY) {
			d = new String("Hard");
		}
		String h = null;
		if (stateHover == 0) {
			h = new String("");
		} else if (stateHover == 1) {
			h = new String("Back");
		} else if (stateHover == 2) {
			h = new String("Play");
		}
		return new String(gameOptionsAssetsPath+"/"+p+d+h+"Click.jpg");
	}

}
