import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JComponent;

/**
 * This class is responsible for loading images, given a string path.
 */
public class GraphicsUtilities {
	/**
	 * Given an absolute path to the image, loads it.
	 * @param path a String containing the path.
	 * @return a BufferedImage object containing the loaded image.
	 */
	public static BufferedImage loadImage(String path) {
		BufferedImage image = null;
		try {
			image = ImageIO.read(new File(path));
		} catch (IOException e) {
			e.printStackTrace();
		}
		if(image == null) throw new RuntimeException("Failed to load image " + path);
		return image;
	}
}
