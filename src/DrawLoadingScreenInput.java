import java.awt.Component;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

/**
 * This class is responsible for dealing with the consequences of 
 * specific mouse movements/actions for the loading screen (DrawLoadingScreen class).
 */
public class DrawLoadingScreenInput extends DrawStateInput {

	private DrawLoadingScreen dls;
	private Component continueRect;
	
	private static final int NONE = 0;
	private static final int CONTINUE_BUTTON = 1;
	
	private int lastButtonPressed = NONE;
	
	public DrawLoadingScreenInput(DrawLoadingScreen dls, Component continueRect) {
		this.dls = dls;
		this.continueRect = continueRect;
	}
	
	/**
	 * Changes the cursor to a hand if its over a button.
	 * Changes the cursor to a default pointer if its over a button.
	 */
	@Override
	public void mouseMoved(MouseEvent e) {
		int mouseX = e.getX();
		int mouseY = e.getY();
		if (mouseOver(continueRect, mouseX, mouseY) && dls.curImage != dls.loadingWaiting) {
			dls.game.setCursorToHand();
		} else {
			dls.game.setCursorToDefault();
		}
		e.consume();
	}

	/**
	 * Changes the background based on the button that is pressed.
	 */
	@Override
	public void mousePressed(MouseEvent e) {
		int mouseX = e.getX();
		int mouseY = e.getY();
		if (mouseOver(continueRect, mouseX, mouseY) && dls.curImage != dls.loadingWaiting) {
			dls.curImage = dls.loadingReadyClick;
			lastButtonPressed = CONTINUE_BUTTON;
		} else if (dls.curImage != dls.loadingWaiting){
			dls.curImage = dls.loadingReady;
		}
		e.consume();
	}

	/**
	 * Changes the background based on the button that is released.
	 */
	@Override
	public void mouseReleased(MouseEvent e) {
		int mouseX = e.getX();
		int mouseY = e.getY();
		if (mouseOver(continueRect, mouseX, mouseY) && dls.curImage != dls.loadingWaiting && lastButtonPressed == CONTINUE_BUTTON) {
			dls.drawMaze.inLoadingScreen = false;
		} else {
			dls.curImage = dls.loadingReady;
			dls.game.setCursorToDefault();
		}
		lastButtonPressed = NONE;
		e.consume();
	}
	
	/**
	 * Implements the interface method but does nothing.
	 */
	@Override
	public void mouseEntered(MouseEvent e) {
	}

	/**
	 * Implements the interface method but does nothing.
	 */
	@Override
	public void mouseExited(MouseEvent e) {
	}

	/**
	 * Implements the interface method but does nothing.
	 */
	@Override
	public void mouseClicked(MouseEvent e) {
	}

	/**
	 * Implements the interface method but does nothing.
	 */
	@Override
	public void mouseDragged(MouseEvent e) {
		
	}
	
	/**
	 * Checks if a mouse is over a specified component 
	 */
	public boolean mouseOver(Component c, int mouseX, int mouseY) {
		return (c.getLocation().x <= mouseX && c.getLocation().x + c.getWidth() >= mouseX &&
			c.getLocation().y <= mouseY && c.getLocation().y + c.getHeight() >= mouseY);
	}
}
