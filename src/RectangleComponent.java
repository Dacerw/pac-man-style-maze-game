import java.awt.Component;

//This class is responsible for creating a rectangle component and updating its size/location.
public class RectangleComponent extends Component {
	public RectangleComponent(int topLeftX, int topLeftY, int width, int height) {
		setLocation(topLeftX, topLeftY);
		setSize(width, height);
	}
	
	//Updates the location of the component
	public void updateLocation(int topLeftX, int topLeftY) {
		setLocation(topLeftX, topLeftY);
	}
	
	//Updates the size of the rectangle component
	public void updateSize(int width, int height) {
		setSize(width, height);
	}
}
