import java.awt.Component;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

/**
 * This class is responsible for dealing with the consequences of 
 * specific mouse movements/actions for the DrawEndScreen class.
 */
public class DrawEndScreenInput extends DrawStateInput {
	private DrawEndScreen de;
	private Component continueButton;
	private int numPlayers;
	
	private static final int NONE = 0;
	private static final int CONTINUE_BUTTON = 1;
	
	private int lastButtonPressed = NONE;
	
	DrawEndScreenInput(DrawEndScreen de, Component continueButton, int numPlayers){
		this.de = de;
		this.continueButton = continueButton;
		this.numPlayers = numPlayers;
	}

	/**
	 * Changes the cursor to a hand if its over a button. 
	 * Changes the cursor to a default pointer if its over a button.
	 */
	@Override
	public void mouseMoved(MouseEvent e) {
		int mouseX = e.getX();
		int mouseY = e.getY();
		if (mouseOver(continueButton, mouseX, mouseY)){
			de.game.setCursorToHand();
		} else {
			de.game.setCursorToDefault();
		}
		e.consume();
	}

	/**
	 * Changes the background based on the button that is pressed.
	 */
	@Override
	public void mousePressed(MouseEvent e) {
		int mouseX = e.getX();
		int mouseY = e.getY();
		if (mouseOver(continueButton, mouseX, mouseY)){
			lastButtonPressed = CONTINUE_BUTTON;
			if (numPlayers == 1){
				de.endState = de.endSingleClick;
			} else {
				de.endState = de.endMultiClick;
			}
		} else {
			if (numPlayers == 1){
				de.endState = de.endSingle;
			} else {
				de.endState = de.endMulti;
			}
		}
	}

	/**
	 * Changes the background based on the button that is released.
	 */
	@Override
	public void mouseReleased(MouseEvent e) {
		int mouseX = e.getX();
		int mouseY = e.getY();
		if (mouseOver(continueButton, mouseX, mouseY) && lastButtonPressed == CONTINUE_BUTTON) {
			de.game.changeState("MENU");
		} else {
			de.game.setCursorToDefault();
			if (numPlayers == 1){
				de.endState = de.endSingle;
			} else {
				de.endState = de.endMulti;
			}
		}
		lastButtonPressed = NONE;
	}

	/**
	 * Implements the interface method but does nothing.
	 */
	@Override
	public void mouseEntered(MouseEvent e) {
	}

	/**
	 * Implements the interface method but does nothing.
	 */
	@Override
	public void mouseExited(MouseEvent e) {
	}
	
	/**
	 * Implements the interface method but does nothing.
	 */
	@Override
	public void mouseClicked(MouseEvent e) {
	}
	
	/**
	 * Implements the interface method but does nothing.
	 */
	@Override
	public void mouseDragged(MouseEvent e) {
	}

	/**
	 * Checks if a mouse is over a specified component 
	 */
	public boolean mouseOver(Component c, int mouseX, int mouseY) {
		return (c.getLocation().x <= mouseX && c.getLocation().x + c.getWidth() >= mouseX &&
			c.getLocation().y <= mouseY && c.getLocation().y + c.getHeight() >= mouseY);
	}
}
