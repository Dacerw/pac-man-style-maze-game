import java.util.ArrayList;


public interface Graph<E> {

	/**
	 * Adds a node to the graph.
	 * Does nothing if an identical node already exists.
	 * @param node The node to be added.
	 */
	void addNode(E node);
	
	/**
	 * Removes a node from the graph.
	 * Does nothing if no such node exists.
	 * @param node The node to be removed.
	 */
	void removeNode(E node);
	
	/**
	 * Adds an edge to the graph given two endpoints.
	 * If either node doesn't exist, adds them to the graph.
	 * @param node1 An endpoint of the edge.
	 * @param node2 An endpoint of the edge.
	 */
	void addEdge(E node1, E node2);
	
	/**
	 * Removes an edge from the graph given two endpoints.
	 * Does nothing if no such edge exists.
	 * @param node1 An endpoint of the edge.
	 * @param node2 An endpoint of the edge.
	 */
	void removeEdge(E node1, E node2);
	
	/**
	 * Checks if a given node exists in the graph
	 * @param node The node to be checked.
	 * @return a boolean value containing the answer.
	 */
	boolean nodeExists(E node);
	
	/**
	 * Checks if a given edge exists in the graph.
	 * @param node1 An endpoint of the edge.
	 * @param node2 An endpoint of the edge.
	 * @return a boolean value containing the answer.
	 */
	boolean edgeExists(E node1, E node2);

	/**
	 * Returns an ArrayList of all the nodes in the graph.
	 * @return the answer to the query.
	 */
	ArrayList<E> getNodes();
	
	/**
	 * Returns an ArrayList of all nodes which are neighbours to a specified node.
	 * @param node A node in the graph to which neighbours will be obtained.
	 * @return the answer to the query. returns null if the specified node does not exist.
	 */
	ArrayList<E> getNeighbours(E node);
}
