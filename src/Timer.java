
/**
 * A class used to get the time elapsed since an event.
 */
public class Timer {
	double eventTime;
	
	/**
	 * Constructs the Timer class. Note that the start or update methods must be
	 * used to signify the start of an event.
	 */
	public Timer(){
		eventTime = 0;
	}
	
	/**
	 * Uses the update method to signify the start of an event.
	 */
	public void start(){
		update();
	}
	
	/**
	 * Stores the current system time, signifying the start of an event.
	 */
	public void update(){
		eventTime = System.nanoTime();
	}
	
	/**
	 * Gets the time elapsed, in nanoseconds, since the last call to update() or start().
	 * @return a double containing the time elapsed, in nanoseconds.
	 */
	public double getTime(){
		return System.nanoTime()-eventTime;
	}
	
	/**
	 * Gets the time elapsed, in seconds, since the last call to update() or start().
	 * @return a double containing the time elapsed, in seconds.
	 */
	public double getTimeS(){
		return (System.nanoTime()-eventTime)/1000000000.0;
	}
}
