import java.awt.Graphics;

/**
 * This interface is used to render screens/dispose resources 
 * for all classes that are responsible for drawing screens.
 */
public interface DrawState {
	
	/**
	 * Displays graphics for a particular screen
	 */
	public void render();
	
	/**
	 * Destroys a particular screen's resources
	 */
	public void dispose();
}
