import java.awt.Component;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

/**
 * This class is responsible for dealing with the consequences of 
 * specific mouse movements/actions for the game options screen (DrawGameOptions class).
 */
public class DrawGameOptionsInput extends DrawStateInput {
	private DrawGameOptions dgo;
	private Component easyButton;
	private Component mediumButton;
	private Component hardButton;
	private Component singleplayerButton;
	private Component multiplayerButton;
	private Component backButton;
	private Component playButton;
	
	private static final int NONE = 0;
	private static final int EASY_BUTTON = 1;
	private static final int MEDIUM_BUTTON = 2;
	private static final int HARD_BUTTON = 3;
	private static final int SP_BUTTON = 4;
	private static final int MP_BUTTON = 5;
	private static final int BACK_BUTTON = 6;
	private static final int PLAY_BUTTON = 7;
	
	private int lastButtonPressed = NONE;
	
	public DrawGameOptionsInput(DrawGameOptions dgo, Component easyButton, Component mediumButton, Component hardButton, 
			                    Component singleplayerButton, Component multiplayerButton, Component back, Component quit) {
		this.dgo = dgo;
		this.easyButton = easyButton;
		this.mediumButton = mediumButton;
		this.hardButton = hardButton;
		this.singleplayerButton = singleplayerButton;
		this.multiplayerButton = multiplayerButton;
		this.backButton = back;
		this.playButton = quit;
	}

	/**
	 * Changes the cursor to a hand if its over a button.
	 * Changes the cursor to a default pointer if its over a button.
	 */
	@Override
	public void mouseMoved(MouseEvent e) {
		int mouseX = e.getX();
		int mouseY = e.getY();
		if (mouseOver(easyButton, mouseX, mouseY) || mouseOver(mediumButton, mouseX, mouseY) ||
			mouseOver(hardButton, mouseX, mouseY) || mouseOver(singleplayerButton, mouseX, mouseY) ||
			mouseOver(multiplayerButton, mouseX, mouseY) || mouseOver(backButton, mouseX, mouseY) ||
			mouseOver(playButton, mouseX, mouseY)) {
			dgo.game.setCursorToHand();
		} else {
			dgo.game.setCursorToDefault();
		}
		e.consume();
	}

	/**
	 * Changes the background based on the button that is pressed.
	 */
	@Override
	public void mousePressed(MouseEvent e) {
		int mouseX = e.getX();
		int mouseY = e.getY();
		if (mouseOver(easyButton, mouseX, mouseY)) {
			dgo.provDifficulty = GameOptions.EASY_DIFFICULTY;
			lastButtonPressed = EASY_BUTTON; 
		} else if (mouseOver(mediumButton, mouseX, mouseY)) {
			dgo.provDifficulty = GameOptions.MEDIUM_DIFFICULTY;
			lastButtonPressed = MEDIUM_BUTTON;
		} else if (mouseOver(hardButton, mouseX, mouseY)) {
			dgo.provDifficulty = GameOptions.HARD_DIFFICULTY;
			lastButtonPressed = HARD_BUTTON;
		} else if (mouseOver(singleplayerButton, mouseX, mouseY)) {
			dgo.provNumPlayers = 1;
			lastButtonPressed = SP_BUTTON;
		} else if (mouseOver(multiplayerButton, mouseX, mouseY)) {
			dgo.provNumPlayers = 2;
			lastButtonPressed = MP_BUTTON;
		} else if (mouseOver(backButton, mouseX, mouseY)) {
			dgo.stateHover = 1;
			lastButtonPressed = BACK_BUTTON;
		} else if (mouseOver(playButton, mouseX, mouseY)) {
			dgo.stateHover = 2;
			lastButtonPressed = PLAY_BUTTON;
		} 
		e.consume();
	}

	/**
	 * Changes the background based on the button that is released.
	 */
	@Override
	public void mouseReleased(MouseEvent e) {
		int mouseX = e.getX();
		int mouseY = e.getY();
		if (mouseOver(easyButton, mouseX, mouseY) && lastButtonPressed == EASY_BUTTON) {
			dgo.game.getGameOptions().setDifficulty(GameOptions.EASY_DIFFICULTY);
			dgo.provDifficulty = GameOptions.EASY_DIFFICULTY;
		} else if (mouseOver(mediumButton, mouseX, mouseY) && lastButtonPressed == MEDIUM_BUTTON) {
			dgo.game.getGameOptions().setDifficulty(GameOptions.MEDIUM_DIFFICULTY);
			dgo.provDifficulty = GameOptions.MEDIUM_DIFFICULTY;
		} else if (mouseOver(hardButton, mouseX, mouseY) && lastButtonPressed == HARD_BUTTON) {
			dgo.game.getGameOptions().setDifficulty(GameOptions.HARD_DIFFICULTY);
			dgo.provDifficulty = GameOptions.HARD_DIFFICULTY;
		} else if (mouseOver(singleplayerButton, mouseX, mouseY) && lastButtonPressed == SP_BUTTON) {
			dgo.provNumPlayers = 1;
			dgo.game.getGameOptions().setNumPlayers(1);
		} else if (mouseOver(multiplayerButton, mouseX, mouseY) && lastButtonPressed == MP_BUTTON) {
			dgo.provNumPlayers = 2;
			dgo.game.getGameOptions().setNumPlayers(2);
		} else if (mouseOver(backButton, mouseX, mouseY) && lastButtonPressed == BACK_BUTTON) {
			dgo.game.changeState("MENU");
		} else if (mouseOver(playButton, mouseX, mouseY) && lastButtonPressed == PLAY_BUTTON) {
			dgo.game.changeState("GAME_MAZE");
		} else {
			dgo.provNumPlayers = dgo.game.getGameOptions().getNumPlayers();
			dgo.provDifficulty = dgo.game.getGameOptions().getDifficulty();
			dgo.stateHover = 0;
			dgo.game.setCursorToDefault();
		}
		lastButtonPressed = NONE;
		e.consume();
	}

	/**
	 * Implements the interface method but does nothing.
	 */
	@Override
	public void mouseEntered(MouseEvent e) {
	}

	/**
	 * Implements the interface method but does nothing.
	 */
	@Override
	public void mouseExited(MouseEvent e) {
	}
	
	/**
	 * Implements the interface method but does nothing.
	 */
	@Override
	public void mouseClicked(MouseEvent e) {
	}
	
	/**
	 * Implements the interface method but does nothing.
	 */
	@Override
	public void mouseDragged(MouseEvent e) {
	}

	/**
	 * Checks if a mouse is over a specified component 
	 */
	public boolean mouseOver(Component c, int mouseX, int mouseY) {
		return (c.getLocation().x <= mouseX && c.getLocation().x + c.getWidth() >= mouseX &&
			c.getLocation().y <= mouseY && c.getLocation().y + c.getHeight() >= mouseY);
	}
}
