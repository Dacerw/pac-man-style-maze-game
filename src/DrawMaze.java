import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyListener;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Implements the screen for displaying the maze. 
 * Note that this class also contains a loading screen which is visible while this class
 * is loading its assets.
 */
public class DrawMaze implements DrawState, Runnable {
	private static final String mazeScreenAssetsPath = new String("resources/midgame screen assets/");
	private static final String mazeTilesPath = new String("resources/maze tiles/");
	private static final String spritesAssetsPath = new String("resources/sprite assets/");
	
	private int wWidth;
	private int wHeight;
	private Thread thread;
	
	public Game game;
	private DrawMazeInput mazeMl;
	
	private Maze maze;
	private String graphicsQuality;
	private HashMap<String, BufferedImage> mazeTiles = new HashMap<String, BufferedImage>();
	private HashMap<String, BufferedImage> sprites = new HashMap<String, BufferedImage>();
	private ArrayList<KeyListener> keyListeners = new ArrayList<KeyListener>();
	
	private RectangleComponent settings;
	private RectangleComponent quit;
	
	public BufferedImage singleInfo;
	public BufferedImage singleClickSettings;
	public BufferedImage singleClickQuit;
	public BufferedImage multiInfo;
	public BufferedImage multiClickSettings;
	public BufferedImage multiClickQuit;
	public BufferedImage currPlayerInfoState;
	public BufferedImage endSingle;
	public BufferedImage endMulti;
	
	public BufferedImage sprite0;
	public BufferedImage sprite1;
	
	public boolean isLoading = true;
	public boolean inLoadingScreen;
	public boolean wasLoading = true;
	private DrawLoadingScreen loadingScreen;
	
	public DrawMaze(Game game) {
		this.game = game;
		wWidth = game.getWindowWidth();
		wHeight = game.getWindowHeight();
		
		graphicsQuality = game.getSettings().getGraphicsQuality();
		maze = game.getMaze();
		
		inLoadingScreen = true;
		isLoading = true;
		loadingScreen = new DrawLoadingScreen(game, this);
		thread = new Thread(this, "Draw maze");
		thread.start();
		
		loadMazeTiles();
		loadSpritesAssets();
		loadPlayerInfoAssets();
		
		isLoading = false;
		if (game.getGameOptions().getNumPlayers() == 1){
			currPlayerInfoState = singleInfo;
		} else {
			currPlayerInfoState = multiInfo;
		}
		
		settings = new RectangleComponent(wWidth*3/5 + wWidth*2/5*23/77, wHeight*2/3, wWidth*5/11*2/5, wHeight*14/153);
		quit = new RectangleComponent(wWidth*3/5 + wWidth*2/5*23/77, wHeight*14/17, wWidth*5/11*2/5, wHeight*14/153);
		
		mazeMl = new DrawMazeInput(this, settings, quit);
	}
	
	@Override
	public void run() {
		while (true) {
			if (inLoadingScreen) {
				loadingScreen.render();
			}
		}
	}
	
	/**
	 * Implementation of the DrawState method.
	 */
	public void render() {
		if (!game.isDisplaying) return;
		if (inLoadingScreen) return;
		//If this is the first time this method has been called since the loading screen
		//has exited, set up the state to be able to render the maze.
		else if (wasLoading) {
			loadingScreen.dispose();
			game.getWindow().addMouseListener(mazeMl);
			game.getWindow().addMouseMotionListener(mazeMl);
			wasLoading = false;
		}
		Window window = game.getWindow();
		
		wWidth = game.getWindowWidth();
		wHeight = game.getWindowHeight();
		
		BufferStrategy bs = window.getBufferStrategy();
		Graphics g = bs.getDrawGraphics();
		g.fillRect(0,0,wWidth,wHeight);
		
		renderMaze(wWidth, wHeight, g);
		renderPlayerScreen(wWidth, wHeight, g);
		
		if (window.getBufferStrategy() != null) {
			g.dispose();
			bs.show();
		}
	}
	
	/**
	 * This method renders the portion to the right of the screen containing information about the state
	 * of the game, and buttons for changing settings and exit to menu screen.
	 * @param wWidth an integer containing the width of the window to which this method draws.
	 * @param wHeight an integer containing the height of the window to which this method draws.
	 * @param g a Graphics object used to draw to the window.
	 */
	private void renderPlayerScreen(int wWidth, int wHeight, Graphics g){
		g.drawImage(currPlayerInfoState, wWidth*3/5, 0, wWidth*2/5, wHeight, null);
		Graphics2D g2 = (Graphics2D)g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        
        settings.updateLocation(wWidth*3/5 + wWidth*2/5*23/77, wHeight*2/3);
		settings.updateSize(wWidth*5/11*2/5, wHeight*14/153);
		quit.updateLocation(wWidth*3/5 + wWidth*2/5*23/77, wHeight*14/17);
		quit.updateSize(wWidth*5/11*2/5, wHeight*14/153);
        
        int fontSize = (wHeight+wWidth)/50;
        g2.setColor(Color.RED);
        Font myFont = new Font("Arial", Font.BOLD, fontSize);
        g2.setFont(myFont);
        
        if (game.getGameOptions().getNumPlayers() == 1) {
        	renderSpScreen(g2); 
        } else {
        	renderMpScreen(g2);
        }
	}
	
	/**
	 * Used by renderPlayerScreen to draw part of the screen in the event of a singleplayer game.
	 * @param g2 a Graphics2D object used to draw to the window.
	 */
	private void renderSpScreen(Graphics2D g2) {
		g2.drawString("Player", wWidth*19/25, wHeight/13);
		
		g2.drawImage(sprite0, wWidth*51/67, wHeight*11/80, wWidth/10, wHeight*3/13, null);
		
		g2.setColor(Color.BLACK);
        g2.drawString(Integer.toString(game.getPlayer(1).getScore()),wWidth*39/50, wHeight*21/41);
	}
	
	/**
	 * Used by renderPlayerScreen to draw part of the screen in the event of a multiplayer game.
	 * @param g2 a Graphics2D object used to draw to the window.
	 */
	private void renderMpScreen(Graphics2D g2) {
		g2.drawString("Player 1",wWidth*15/23, wHeight/13);
        g2.setColor(Color.BLUE);
        g2.drawString("Player 2",wWidth*9/11, wHeight/13); 
        
        g2.drawImage(sprite0, wWidth*45/67, wHeight*11/80, wWidth/10, wHeight*3/13, null);
        g2.drawImage(sprite1, wWidth*41/49, wHeight*11/80, wWidth/10, wHeight*3/13, null);
        
        g2.setColor(Color.BLACK);
        g2.drawString(Integer.toString(game.getPlayer(1).getScore()),wWidth*30/43, wHeight*21/41);
        g2.drawString(Integer.toString(game.getPlayer(2).getScore()),wWidth*43/50, wHeight*21/41);
	}
	
	/**
	 * This method renders the main part of the screen, containing the maze and entities.
	 * @param wWidth an integer containing the width of the window to which this method draws.
	 * @param wHeight an integer containing the height of the window to which this method draws.
	 * @param g a Graphics object used to draw to the window.
	 */
	private void renderMaze(int wWidth, int wHeight, Graphics g){
		Graph<Point> mazeGraph = maze.getMaze();
		int tileSizeX = wWidth*3/5/maze.getWidth();
		int tileSizeY = wHeight/maze.getHeight();
		//first, draw the maze.
		for (int y = 0; y < maze.getHeight(); y++) {
			for (int x = 0; x < maze.getWidth(); x++) {
				String tileCode = getTileCode(x, y, mazeGraph);
				g.drawImage(mazeTiles.get(tileCode), x*tileSizeX, y*tileSizeY, tileSizeX, tileSizeY, null);
			}
		}
		List<Entity> entities = game.getEntities();
		//second, draw the entities over the maze.
		for (Entity e : entities) {
			String runningState = e.getRunningState();
			BufferedImage sprite = sprites.get(runningState);
			if (sprite == null) {
				sprite = GraphicsUtilities.loadImage(runningState);
				sprites.put(runningState, sprite);
			}
			double entityWidth = e.getSizeRatioX()*tileSizeX;
			double entityHeight = e.getSizeRatioY()*tileSizeY;
			//entity methods return the central position of the entity.
			//the following code determines the top left of the entity.
			double entityTopLeftX = e.getXPos()*tileSizeX/(double)Entity.TILE_SIZE-entityWidth/2;
			double entityTopLeftY = e.getYPos()*tileSizeY/(double)Entity.TILE_SIZE-entityHeight/2;
			g.drawImage(sprite, (int)entityTopLeftX, (int)entityTopLeftY, (int)entityWidth, (int)entityHeight, null);
		}
	}
	
	@Override
	public void dispose() {
	  	game.getWindow().removeMouseListener(mazeMl);
		game.getWindow().removeMouseMotionListener(mazeMl);
		for (KeyListener kl : keyListeners) {
			game.getWindow().removeKeyListener(kl);
		}
		game.gameLogic.isRunning = false;
	}
	
	/**
	 * Loads the background tiles of the maze.
	 */
	private void loadMazeTiles() {
		String path = null;
		if (graphicsQuality.equalsIgnoreCase("Low")) {
			path = new String(mazeTilesPath + "Small Images");
		} else if (graphicsQuality.equalsIgnoreCase("Medium")) {
			path = new String(mazeTilesPath + "Medium Images");
		} else if (graphicsQuality.equalsIgnoreCase("High")) {
			path = new String(mazeTilesPath + "Big Images");
		}
		assert(path != null);
		File folder = new File(path);
		File[] listOfFiles = folder.listFiles();
		for (File f : listOfFiles) {
			String pathToFile = f.getName();
			String tileCode = "";
			//In resources, the tiles follow the convention "grass-tileCode.jpg".
			//this extracts the tileCode portion of the file name.
			if (pathToFile.split("\\-")[0].equalsIgnoreCase("grass")) {
				tileCode += pathToFile.split("\\-")[1].split("\\.")[0];
				mazeTiles.put(tileCode, GraphicsUtilities.loadImage(path + "/" + pathToFile));
			}
		}
	}
	
	/**
	 * Loads the sprites used by the player screen.
	 */
	private void loadSpritesAssets() {
		sprite0 = GraphicsUtilities.loadImage(spritesAssetsPath + "spr1_d0.png");
		sprite1 = GraphicsUtilities.loadImage(spritesAssetsPath + "spr2_d0.png");
	}
	
	/**
	 * Loads the background images used for the player screen.
	 */
	private void loadPlayerInfoAssets(){
		
		singleInfo = GraphicsUtilities.loadImage(mazeScreenAssetsPath + "SingleInfo.jpg");
		singleClickSettings = GraphicsUtilities.loadImage(mazeScreenAssetsPath + "SingleInfoSettingsClick.jpg");
		singleClickQuit = GraphicsUtilities.loadImage(mazeScreenAssetsPath + "SingleInfoQuitClick.jpg");
		multiInfo = GraphicsUtilities.loadImage(mazeScreenAssetsPath + "MultiInfo.jpg");
		multiClickSettings = GraphicsUtilities.loadImage(mazeScreenAssetsPath + "MultiInfoSettingsClick.jpg");
		multiClickQuit = GraphicsUtilities.loadImage(mazeScreenAssetsPath + "MultiInfoQuitClick.jpg");
	}
	
	/**
	 * Adds a key listener to this DrawState implementation.
	 * @param kl a KeyListener object.
	 */
	public void addKeyListener(KeyListener kl) {
		game.getWindow().addKeyListener(kl);
		keyListeners.add(kl);
	}
	
	/**
	 * Given a point, and the representation of the maze, retrieves the tile code for the point,
	 * based on what its neighbours are.
	 * @param x the x-coordinate of the point.
	 * @param y the y-coordinate of the point.
	 * @param mazeGraph a Graph\<Point\> object containing the maze representation.
	 * @return a String containing the tile code.
	 */
	private String getTileCode(int x, int y, Graph<Point> mazeGraph) {
		ArrayList<Point> neighbours = mazeGraph.getNeighbours(new Point(x, y));
		if (neighbours.size() == 0) return new String("N");
		String ret = "";
		if (neighbours.contains(new Point(x+1, y))) ret += "R";
		if (neighbours.contains(new Point(x, y+1)) || (y == maze.getHeight()-1 && x == maze.getWidth()-1)) ret += "D";
		if (neighbours.contains(new Point(x-1, y))) ret += "L";
		if (neighbours.contains(new Point(x, y-1))) ret += "U";
		return ret;
	}
	
	/**
	 * Retrieves whether this screen is currently loading its resources.
	 * @return
	 */
	public boolean getIsLoadingScreen() {
		return inLoadingScreen;
	}
	
}
