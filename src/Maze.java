import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Random;


/**
 * Creates a representation of a maze using the Graph interface, with type Point.
 * Also provides methods to see if an entity collides with the walls of the maze.
 */
public class Maze {
	
	private Graph<Point> maze;
	private int width;
	private int height; 
	
	/**
	 * Constructs a maze given a width and height.
	 * @param width an integer containing the width.
	 * @param height an integer containing the height.
	 */
	public Maze(int width, int height) {
		this.width = width;
		this.height = height;
		maze = generateMaze();
	}
	
	/**
	 * This method contains the logic for generating the maze.
	 * @return A Graph\<Point\> containing the representation of the maze.
	 */
	private Graph<Point> generateMaze() {
		Graph<Point> maze = new UnweightedUndirectedGraph<Point>();
		HashSet<Point> visited = new HashSet<Point>();
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				maze.addNode(new Point(x, y));
			}
		}
		
		//Generate the initial maze using a basic dfs strategy, expanding from the middle. 
		Point initialPoint = new Point(width/2, height/2);
		maze.addNode(initialPoint);
		dfsGenerate(maze, visited, initialPoint);
		
		//Now add bridges to the maze in a sensible position.
		int numBridges = width*height/100;
		Random rnd = new Random(System.nanoTime());
		for (int i = 0; i < numBridges; i++) {
			int xLeftOrRight = rnd.nextInt(3);
			int yTopOrDown = rnd.nextInt(3);
			int x = rnd.nextInt(width/2);
			int y = rnd.nextInt(height/2);
			//Check if the bridge will be generated in the right half (33% chance)
			if (xLeftOrRight == 2) {
				x += width/2;
			}
			//Check if the bridge will be generated in the bottom half (33% chance)
			if (yTopOrDown == 2) {
				y += width/2;
			}
			boolean pointFound = false;
			//Iterate through all points in the maze until it finds a point containing walls to its side
			//in some orientation. 
			for (int j = (x+1)%width; j != x; j = (j+1)%width) {
				if (j == 0 || j == width-1) continue;
				if (pointFound) break;
				for (int k = (y+1)%height; k != y; k = (k+1)%height) {
					//We don't want to generate a bridge to the outside of the maze, nor generate one in the 
					//bottom right quadrant.
					if (k == 0 || k == height-1 || (k >= height/2 && j >= width/2)) continue;
					if (pointFound) break;
					//Check if a bridge can be built in any orientation(top, down, left, right).
					if (canBuildBridge(maze, new Point(j, k), new Point(j-1, k), new Point(j+1, k), 0, -1)) {
						maze.addEdge(new Point(j, k), new Point(j, k-1));
						pointFound = true;
					} else if (canBuildBridge(maze, new Point(j, k), new Point(j-1, k), new Point(j+1, k), 0, 1)) {
						maze.addEdge(new Point(j, k), new Point(j, k+1));
						pointFound = true;
					} else if (canBuildBridge(maze, new Point(j, k), new Point(j, k-1), new Point(j, k+1), -1, 0)) {
						maze.addEdge(new Point(j, k), new Point(j-1, k));
						pointFound = true;
					} else if (canBuildBridge(maze, new Point(j, k), new Point(j, k-1), new Point(j, k+1), 1, 0)) {
						maze.addEdge(new Point(j, k), new Point(j+1, k));
						pointFound = true;
					}
				}
			}
			if (!pointFound) break;
		}
		
		return maze;
	}
	
	/**
	 * If three given points contain a wall adjacent to them in some orientation, returns true.
	 * Otherwise, returns false.
	 * @param maze a Graph\<Point\> object containing the maze.
	 * @param p1 one of the points.
	 * @param p2 one of the points.
	 * @param p3 one of the points.
	 * @param dx the orientation in the x-axis which will be checked.
	 * @param dy the orientation in the y-axis which will be checked.
	 * @return a boolean value containing the answer.
	 */
	private boolean canBuildBridge(Graph<Point> maze, Point p1, Point p2, Point p3, int dx, int dy) {
		ArrayList<Point> edgeP1 = maze.getNeighbours(p1);
		ArrayList<Point> edgeP2 = maze.getNeighbours(p2);
		ArrayList<Point> edgeP3 = maze.getNeighbours(p3);
		return (!edgeP1.contains(new Point(p1.getX()+dx, p1.getY()+dy)) &&
				!edgeP2.contains(new Point(p2.getX()+dx, p2.getY()+dy)) &&
				!edgeP3.contains(new Point(p3.getX()+dx, p3.getY()+dy)));
	}
	
	/**
	 * Gets the maze representation.
	 * @return a Graph<Point> object containing the maze representation.
	 */
	public Graph<Point> getMaze() {
		return maze;
	}
	
	/**
	 * Gets the maze width.
	 * @return an integer containing the width.
	 */
	public int getWidth() {
		return width;
	}
	
	/**
	 * Gets the maze height.
	 * @return an integer containing the height.
	 */
	public int getHeight() {
		return height;	
	}
	
	/**
	 * Uses a randomized expansion dfs strategy to generate a maze.
	 * @param maze a Graph\<Point\> object to which the maze will be added to.
	 * @param visited a HashSet\<Point\> object containing which grid positions in the maze have been visited.
	 * @param curPoint a Point object containing the point this strategy will expand from.
	 */
	private void dfsGenerate(Graph<Point> maze, HashSet<Point> visited, Point curPoint) {
		visited.add(curPoint);
		ArrayList<Point> potentialNeighbours = getPotentialNeighbours(curPoint);
		Collections.shuffle(potentialNeighbours, new Random(System.nanoTime()));
		for (Point neighbour : potentialNeighbours) {
			if (!visited.contains(neighbour)) {
				maze.addEdge(curPoint, neighbour);
				//As a neighbour can contain a tile outside of the bounds of the maze (representing
				//an exit), we must not expand from these.
				if (neighbour.getY() != height && neighbour.getX() != width)
					dfsGenerate(maze, visited, neighbour);
			}
		}
	}
	
	/**
	 * Gets all the potential neighbours of a given point (up, down, left, right).
	 * A neighbour must be within the bounds of the maze.
	 * @param curPoint a Point object.
	 * @return an ArrayList of Points containing the pontential neighbours of the given point.
	 */
	private ArrayList<Point> getPotentialNeighbours(Point curPoint) {
		int x = curPoint.getX();
		int y = curPoint.getY();
		ArrayList<Point> res = new ArrayList<Point>();
		if (x == width-1 && y == height-1) res.add(new Point(x, y+1));
		if (x - 1 >= 0) res.add(new Point(x-1, y));
		if (x + 1 < width) res.add(new Point(x+1, y));
		if (y - 1 >= 0) res.add(new Point(x, y-1));
		if (y + 1 < height) res.add(new Point(x, y+1));
		return res;
	}
	
	/**
	 * Given information about an entity, returns whether it collides with any walls of the maze.
	 * @param x a double containing its x position.
	 * @param y a double containing its y position.
	 * @param wallRatio a double containing the wall:totalTileSize ratio.
	 * @param entityRatioX a double containing the entitySizeX:totalTileSize ratio.
	 * @param entityRatioY a double containing the entitySizeY:totalTileSize ratio.
	 * @param tileSize a double containing the tile size.
	 * @return
	 */
	public boolean collides(double x, double y, double wallRatio, double entityRatioX, double entityRatioY, int tileSize) {
		//handle case where the entity is in the center of a tile
		double x_pos = x/tileSize;
		double y_pos = y/tileSize;
		wallRatio /= 2;
		entityRatioX *= (double)4/5;
		if (inTile(x_pos, entityRatioX, wallRatio) && inTileFeet(y_pos, entityRatioY, wallRatio)) {
			return false;
		}
		//handle case where the entity is between two tiles.
		else if (inTileFeet(y_pos, entityRatioY, wallRatio)) {
			return !edgeExists((int)x_pos, (int)y_pos, (int)x_pos+((x_pos-Math.floor(x_pos) > 0.5) ? 1 : -1), (int)y_pos);
		}
		else if (inTile(x_pos, entityRatioX, wallRatio)) {
			return !edgeExists((int)x_pos, (int)y_pos, (int)x_pos, (int)y_pos+((y_pos-Math.floor(y_pos) > 0.5) ? 1 : -1));
		}
		return true;
	}
	
	/**
	 * Determines if an edge exists between two points
	 * @param x1 the x-coordinate of the first point.
	 * @param y1 the y-coordinate of the first point.
	 * @param x2 the x-coordinate of the second point.
	 * @param y2 the y-coordinate of the second point.
	 * @return a boolean value containing true if the edge exists, and false otherwise.
	 */
	private boolean edgeExists(int x1, int y1, int x2, int y2) {
		ArrayList<Point> neighbours = maze.getNeighbours(new Point(x1, y1));
		return (neighbours.contains(new Point(x2, y2)));
	}
	
	/**
	 * Used by collides to check if an entity collides with a wall when inside a certain tile.
	 * This bases the collision based on the feet of the entity, as opposed to the center.
	 * @param pos a double containing the position in an axis relative to the entire tile.
	 * @param entityRatio a double containing the ratio of the entity in an axis relative to the entire tile.
	 * @param wallRatio a double containing the ratio of the wall in an axis relative to the entire tile.
	 * @return a boolean containing true if the entity does not collide, and false otherwise.
	 */
	private boolean inTileFeet(double pos, double entityRatio, double wallRatio) {
		entityRatio *= (double)4/5;
		return (pos-entityRatio/3 > Math.floor(pos) + wallRatio && 
				pos+entityRatio*2/3 < Math.ceil(pos) - wallRatio);
	}
	
	/**
	 * Used by collides to check if an entity collides with a wall when inside a certain tile.
	 * This bases the collision based on the center of the entity, as opposed to the feet.
	 * @param pos a double containing the position in an axis relative to the entire tile.
	 * @param entityRatio a double containing the ratio of the entity in an axis relative to the entire tile.
	 * @param wallRatio a double containing the ratio of the wall in an axis relative to the entire tile.
	 * @return a boolean containing true if the entity does not collide, and false otherwise.
	 */
	private boolean inTile(double pos, double entityRatio, double wallRatio) {
		return (pos-entityRatio/2 > Math.floor(pos) + wallRatio && 
			pos+entityRatio/2 < Math.ceil(pos) - wallRatio);
	}
}
