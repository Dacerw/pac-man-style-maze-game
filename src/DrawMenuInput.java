import java.awt.Component;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

/**
 * This class is responsible for dealing with the consequences of 
 * specific mouse movements/actions for the DrawMenuInput screen.
 */
public class DrawMenuInput extends DrawStateInput {
	
	private DrawMenu dm;

	private Component newGame;
	private Component quit;
	private Component settings;
	
	private static final int NONE = 0;
	private static final int GAME_BUTTON = 1;
	private static final int QUIT_BUTTON = 2;
	private static final int SETTINGS_BUTTON = 3;
	
	private int lastButtonPressed = NONE;
	
	public DrawMenuInput(DrawMenu dm, Component newGame, Component quit, Component settings) {
		this.dm = dm;
		this.newGame = newGame;
		this.quit = quit;
		this.settings = settings;
	}
	
	/**
	 * Changes the cursor to a hand if its over a button.
	 * Changes the cursor to a default pointer if its over a button.
	 */
	@Override
	public void mouseMoved(MouseEvent e) {
		int mouseX = e.getX();
		int mouseY = e.getY();
		if (mouseOver(newGame, mouseX, mouseY) || mouseOver(settings, mouseX, mouseY) 
	     || mouseOver(quit, mouseX, mouseY)) {
			dm.game.setCursorToHand();
		} else {
			dm.game.setCursorToDefault();
		}
	}

	/**
	 * Changes the background based on the button that is pressed.
	 */
	@Override
	public void mousePressed(MouseEvent e) {
		int mouseX = e.getX();
		int mouseY = e.getY();
		if (mouseOver(newGame, mouseX, mouseY)) {
			dm.curMenuState = dm.menuBackgroundNewGame;
			lastButtonPressed = GAME_BUTTON;
		} else if (mouseOver(settings, mouseX, mouseY)) {
			dm.curMenuState = dm.menuBackgroundSettings;
			lastButtonPressed = SETTINGS_BUTTON;
		} else if (mouseOver(quit, mouseX, mouseY)) {
			dm.curMenuState = dm.menuBackgroundQuit;
			lastButtonPressed = QUIT_BUTTON;
		} else {
			dm.curMenuState = dm.menuBackgroundDefault;
		}
	}

	/**
	 * Changes the background based on the button that is released.
	 */
	@Override
	public void mouseReleased(MouseEvent e) {
		int mouseX = e.getX();
		int mouseY = e.getY();
		if (mouseOver(newGame, mouseX, mouseY) && lastButtonPressed == GAME_BUTTON) {
			dm.game.changeState("GAME");
		} else if (mouseOver(settings, mouseX, mouseY) && lastButtonPressed == SETTINGS_BUTTON) {
			dm.game.changeState("SETTINGS");
			dm.curMenuState = dm.menuBackgroundDefault;
		} else if (mouseOver(quit, mouseX, mouseY) && lastButtonPressed == QUIT_BUTTON) {
			dm.game.changeState("EXIT");
		} else {
			dm.game.setCursorToDefault();
			dm.curMenuState = dm.menuBackgroundDefault;
		}
		lastButtonPressed = NONE;
	}

	/**
	 * Implements the interface method but does nothing.
	 */
	@Override
	public void mouseEntered(MouseEvent e) {
	}

	/**
	 * Implements the interface method but does nothing.
	 */
	@Override
	public void mouseExited(MouseEvent e) {
	}

	/**
	 * Implements the interface method but does nothing.
	 */
	@Override
	public void mouseClicked(MouseEvent e) {
	}

	/**
	 * Implements the interface method but does nothing.
	 */
	@Override
	public void mouseDragged(MouseEvent e) {
	}

	/**
	 * Checks if a mouse is over a specified component 
	 */
	public boolean mouseOver(Component c, int mouseX, int mouseY) {
		return (c.getLocation().x <= mouseX && c.getLocation().x + c.getWidth() >= mouseX &&
			c.getLocation().y <= mouseY && c.getLocation().y + c.getHeight() >= mouseY);
	}
}
