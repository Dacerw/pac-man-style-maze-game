import java.util.HashMap;

/**
 * Stores the mapped player keys for movement in the game.
 */
public class KeyboardSettings {
	private String upInput;
	private String downInput;
	private String leftInput;
	private String rightInput;
	private String keyboardInputString;
	
	private static HashMap<String, Integer> keyStringToInt; 
	
	KeyboardSettings(String up, String down, String left, String right){
		this.upInput = up;
		this.downInput = down;
		this.leftInput = left;
		this.rightInput = right;
		this.keyboardInputString = upInput + " " + downInput + " " + 
								   leftInput + " " + rightInput;
		
		keyStringToInt = new HashMap<String, Integer>();
		loadHashMapValues();
	}
	
	/**
	 * Gets the combined set of mapped players keys.
	 * @return a String containing the combined set.
	 */
	public String getKeyboardSettings(){
		return keyboardInputString;
	}
	
	/**
	 * Maps the keys to given values.
	 * @param up a String containing the map for moving up
	 * @param down a String containing the map for moving down
	 * @param left a String containing the map for moving left
	 * @param right a String containing the map for moving right
	 */
	public void setKeyboardSettings(String up, String down, String left, String right){
		this.upInput = up;
		this.downInput = down;
		this.leftInput = left;
		this.rightInput = right;
		this.keyboardInputString = upInput + " " + downInput + " " + 
			    				   leftInput + " " + rightInput;
	}
	
	/**
	 * Loads values of strings into keyStringToInt, based on their keycode.
	 */
	private void loadHashMapValues(){
		keyStringToInt.put("left", 37);
		keyStringToInt.put("up", 38);
		keyStringToInt.put("right", 39);
		keyStringToInt.put("down", 40);
		keyStringToInt.put("a", 65);
		keyStringToInt.put("b", 66);
		keyStringToInt.put("c", 67);
		keyStringToInt.put("d", 68);
		keyStringToInt.put("e", 69);
		keyStringToInt.put("f", 70);
		keyStringToInt.put("g", 71);
		keyStringToInt.put("h", 72);
		keyStringToInt.put("i", 73);
		keyStringToInt.put("j", 74);
		keyStringToInt.put("k", 75);
		keyStringToInt.put("l", 76);
		keyStringToInt.put("m", 77);
		keyStringToInt.put("n", 78);
		keyStringToInt.put("o", 79);
		keyStringToInt.put("p", 80);
		keyStringToInt.put("q", 81);
		keyStringToInt.put("r", 82);
		keyStringToInt.put("s", 83);
		keyStringToInt.put("t", 84);
		keyStringToInt.put("u", 85);
		keyStringToInt.put("v", 86);
		keyStringToInt.put("w", 87);
		keyStringToInt.put("x", 88);
		keyStringToInt.put("y", 89);
		keyStringToInt.put("z", 90);
	}
	
	/**
	 * Gets the keycode for moving up.
	 * @return an integer containing the keycode.
	 */
	public int getUp(){
		return keyStringToInt.get(upInput);
	}
	
	/**
	 * Gets the keycode for moving down.
	 * @return an integer containing the keycode.
	 */
	public int getDown(){
		return keyStringToInt.get(downInput);
	}
	
	/**
	 * Gets the keycode for moving left.
	 * @return an integer containing the keycode.
	 */
	public int getLeft(){
		return keyStringToInt.get(leftInput);
	}
	
	/**
	 * Gets the keycode for moving right.
	 * @return an integer containing the keycode.
	 */
	public int getRight(){
		return keyStringToInt.get(rightInput);
	}
}
