
/**
 * Determines what OS the game is being run on.
 */
public class OsUtils {
	private static String OS = null;
	/**
	 * Determines the OS name in lower case letters.
	 * @return a String object containing the OS name in lowercase letters.
	 */
	public static String getOsName() {
		if (OS == null) {
			return System.getProperty("os.name").toLowerCase();
		}
		return OS;
	}
	
	/**
	 * Determines if the OS is windows.
	 * @return a boolean value containing the answer.
	 */
	public static boolean isWindows() {
		return (getOsName().indexOf("win") >= 0);
	}
	
	/**
	 * Determines if the OS is mac.
	 * @return a boolean value containing the answer.
	 */
	public static boolean isMac() {
		return (getOsName().indexOf("mac") >= 0);
	}
	
	/**
	 * Determines if the OS is unix.
	 * @return a boolean value containing the answer.
	 */
	public static boolean isUnix() {
		return (getOsName().indexOf("nix") >= 0 || getOsName().indexOf("nux") >= 0 || getOsName().indexOf("aix") > 0 );
	}
}
