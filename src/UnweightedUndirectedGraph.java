import java.util.ArrayList;
import java.util.HashMap;

/**
 * Implements the Graph\<E\> interface to represent an unweighted, undirected graph.
 * @param <E> A data type representing nodes in the graph.
 */
public class UnweightedUndirectedGraph<E> implements Graph<E> {

	HashMap<E, ArrayList<E> > adj = new HashMap<E, ArrayList<E> >();
	
	@Override
	public void addNode(E node) {
		if (!adj.containsKey(node)) {
			adj.put(node, new ArrayList<E>());
		}
	}

	/**
	 * Note that this is an O(n^2) method in the event of a complete graph, as all edges to the specified node
	 * must be deleted. For a graph with a small branching factor, this will be O(1).
	 */
	@Override
	public void removeNode(E node) {
		ArrayList<E> neighbours = getNeighbours(node);
		for (E neighbour : neighbours) {
			adj.get(neighbour).remove(node);
		}
		adj.remove(node);
	}

	/**
	 * Note that this implementation adds the edge even if an identical edge exists.
	 */
	@Override
	public void addEdge(E node1, E node2) {
		if (!adj.containsKey(node1)) addNode(node1);
		if (!adj.containsKey(node2)) addNode(node2);
		adj.get(node1).add(node2);
		adj.get(node2).add(node1);
	}

	/**
	 * Uses the List.remove() method, which only deletes one occurrence of the edge.
	 */
	public void removeEdge(E node1, E node2) {
		if (!adj.containsKey(node1)) return;
		if (!adj.containsKey(node2)) return;
		adj.get(node1).remove(node2);
		adj.get(node2).remove(node1);
	}

	@Override
	public boolean nodeExists(E node) {
		return (adj.containsKey(node));
	}

	@Override
	public boolean edgeExists(E node1, E node2) {
		if (!nodeExists(node1)) return false;
		return (adj.get(node1).contains(node2));
	}

	@Override
	public ArrayList<E> getNodes() {
		return new ArrayList<E>(adj.keySet());
	}

	@Override
	public ArrayList<E> getNeighbours(E node) {
		if (!nodeExists(node)) return null;
		return new ArrayList<E>(adj.get(node));
	}

		
}
