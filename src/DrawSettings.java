import java.awt.Dimension;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

/**
 * This class is responsible for drawing the settings screen, when the settings button is selected.
 * It is also responsible for altering the window resolution and graphics quality.
 */
public class DrawSettings extends JFrame implements DrawState {
	private int wWidth;
	private int wHeight;
	
	public Game game;
	
	private boolean isFullscreen;
	private boolean scoreCountdown;

	private JPanel panel;
	
	private JTabbedPane tabbedPane;
	private JLayeredPane graphicsPane;
	
	private JLabel resolutionsHeading;
	private JComboBox resolutionCombo;
	private Vector<String> resolutions;
	
	private JLabel graphicsQualityHeading;
	private JComboBox graphicsQualityCombo;
	private Vector<String> graphicsQualities;
	
	private JCheckBox fullscreenCheckbox;
	private JCheckBox scoreCountdownCheckbox;
	
	public DrawSettings(Game g) {
		this.game = g;
		isFullscreen = g.getSettings().getIsFullscreen();
		wWidth = g.getSettings().getResolutionWidth();
		wHeight = g.getSettings().getResolutionHeight();
		scoreCountdown = g.getSettings().getScoreCountdown();
		createJFrame();
		setVisible(true);
	}
	
	@Override
	public void render() {
	}

	@Override
	public void dispose() {
		setVisible(false);
	}
	
	/**
	 * Creates the settings window itself
	 */
	private void createJFrame() {
		//Sets dimensions of the panel and panes
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				game.setSettingsWindowOpen(false);
				setVisible(false);
			}
		});
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension panelDim = new Dimension(dim.width /2, dim.height / 2);
		setPreferredSize(panelDim);
		setMinimumSize(panelDim);
		setMaximumSize(panelDim);
		this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
		
		int pWidth = this.getSize().width;
		int pHeight = this.getSize().height;
		
		panel = new JPanel();
		setContentPane(panel);
		panel.setLayout(null);
		
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 0, pWidth, pHeight - pHeight / 6);
		panel.add(tabbedPane);

		graphicsPane = new JLayeredPane();
		tabbedPane.addTab("Settings", null, graphicsPane, null);
		
		//Removes the full screen functionality for Macs as it is unaesthetically pleasing
		if (!OsUtils.isMac()) {
			fullscreenCheckbox = new JCheckBox("Fullscreen");
			fullscreenCheckbox.setBounds(pWidth/10, pHeight/10, pWidth/6, pHeight/20);
			graphicsPane.add(fullscreenCheckbox);
			fullscreenCheckbox.setSelected(isFullscreen);
		}
		
		//Creates the resolutions comboBox
		resolutionsHeading = new JLabel("Resolution");
		resolutionsHeading.setBounds(pWidth/10, pHeight*7/47, pWidth*7/20, pHeight/14);
	    graphicsPane.add(resolutionsHeading);
		
		resolutions = getResolutions();
		resolutionCombo = new JComboBox();
		resolutionCombo.setModel(new DefaultComboBoxModel(resolutions));
		resolutionCombo.setBounds(pWidth/10, pHeight/5, pWidth*7/20, pHeight/14);
		graphicsPane.add(resolutionCombo);
		resolutionCombo.setSelectedIndex(resolutions.indexOf(new String(game.getSettings().getResolutionWidth()+"x"+game.getSettings().getResolutionHeight())));
		
		//Creates the Graphics Quality comboBox
		graphicsQualityHeading = new JLabel("Graphics Quality (only takes effect after a maze restarts)");
		graphicsQualityHeading.setBounds(pWidth/10, pHeight*17/60, pWidth*15/20, pHeight/14);
		graphicsQualityHeading.setBounds(pWidth/10, pHeight*17/60, pWidth*11/20, pHeight/14);

	    graphicsPane.add(graphicsQualityHeading);
		
		graphicsQualities = getGraphicsQualities();
		graphicsQualityCombo = new JComboBox();
		graphicsQualityCombo.setModel(new DefaultComboBoxModel(graphicsQualities));
		graphicsQualityCombo.setBounds(pWidth/10, pHeight/3, pWidth*7/20, pHeight/14);
		graphicsPane.add(graphicsQualityCombo);
		graphicsQualityCombo.setSelectedIndex(graphicsQualities.indexOf(new String(game.getSettings().getGraphicsQuality())));
		
		//Creates a checkbox for enabling/disabling score counter
		scoreCountdownCheckbox = new JCheckBox("Disable score countdown");
		scoreCountdownCheckbox.setBounds(pWidth/10, pHeight*13/30, pWidth*1/2, pHeight/20);
		graphicsPane.add(scoreCountdownCheckbox);
		scoreCountdownCheckbox.setSelected(!scoreCountdown);
		
		//Creates a save button
		JButton btnSave = new JButton("Save");
		btnSave.setBounds(pWidth / 5, pHeight * 167 / 200, pWidth / 5, pHeight / 12);
		panel.add(btnSave);
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent a) {
				saveSettings();
			}
		});
		
		//Creates a cancel button
		JButton cancel = new JButton("Cancel");
		cancel.setBounds(pWidth * 3 / 5, pHeight * 167 / 200, pWidth / 5, pHeight / 12);
		panel.add(cancel);
		cancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent a) {
				game.setSettingsWindowOpen(false);
				setVisible(false);
			}
		});
	}

	/**
	 * Passes the window/graphics settings into the GameSettings class so that it can be permanently saved
	 */
	private void saveSettings() {
		//Converts wWidth/wHeight into a string before it is passed into the GameSettings class
		wWidth = Integer.parseInt(resolutions.get(resolutionCombo.getSelectedIndex()).split("x")[0]);
	    wHeight = Integer.parseInt(resolutions.get(resolutionCombo.getSelectedIndex()).split("x")[1]);
		game.getSettings().setResolutionWidth(wWidth);
		game.getSettings().setResolutionHeight(wHeight);
	
		//Ensures that Mac operating systems are never full screen
		if (!OsUtils.isMac()) {
			isFullscreen = fullscreenCheckbox.isSelected();
		} else {
			isFullscreen = false;
		}
		
		//Passes fullscreen, dimension settings into GameSettings class
		game.getSettings().setFullscreen(isFullscreen);
		game.getWindow().hideWindow();
		if (isFullscreen) {
			game.getWindow().setFullscreen();
		} else {
			game.getWindow().setDimension(wWidth, wHeight);
		}
		
		//Passes graphics quality settings into the GameSettings class
		String graphicsQuality = graphicsQualities.get(graphicsQualityCombo.getSelectedIndex());
		game.getSettings().setGraphicsQuality(graphicsQuality);
		
		//Passes the score countdown settings into the GameSettings class
		scoreCountdown = !(scoreCountdownCheckbox.isSelected());
		game.getSettings().setScoreCountdown(scoreCountdown);
		
		game.getSettings().writeSettings();
		
		game.getWindow().showWindow();
		game.setSettingsWindowOpen(false);
		this.setVisible(false);
	}

	/**
	 * Gets a Vector of window resolutions which is used by JComboBox resolutionCombo
	 * @return a Vector\<String\> object containing the possible resolutions in the format
	 * (width)x(height), without parentheses.
	 */
	private Vector<String> getResolutions() {
		Vector<String> resolutions = new Vector<String>();
		GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
		int maxW = gd.getDisplayMode().getWidth();
		int maxH = gd.getDisplayMode().getHeight();
		String possible[] = new String[] { 
				"800x560",
				"1024x600",
				"1024x768",
				"1152x864",
				"1280x720",
				"1280x768",
				"1280x800",
				"1280x960",
				"1280x1024",
				"1360x768",
				"1366x766",
				"1440x900",
				"1600x900",
				"1600x1200",
				"1680x1050",
				"1920x1080",
				"1920x1200",
				"2048x1152",
				"2560x1440",
				"2560x1600"
		};
		for (String s : possible) {
			int width = Integer.parseInt(s.split("x")[0]);
			int height = Integer.parseInt(s.split("x")[1]);
			if (width <= maxW && height <= maxH)
				resolutions.add(s);
		}
		return resolutions;
	}
	
	/**
	 * Gets a Vector of graphics qualities  which is used by JComboBox graphicsQualityCombo
	 * @return a Vector\<String\> object containing the possible graphics qualities for the game.
	 */
	private Vector<String> getGraphicsQualities() {
		Vector<String> ret = new Vector<String>();
		ret.add("Low");
		ret.add("Medium");
		ret.add("High");
		return ret;
	}

}

