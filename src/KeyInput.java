import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Handles keyboard input for the Player.
 */
public class KeyInput implements KeyListener{	

	private Entity entity;
	private KeyboardSettings ks;
	
	public KeyInput(Entity e, KeyboardSettings ks) {
		this.entity = e;
		this.ks = ks;
	}
	
	/**
	 * Implementation of the KeyListener method
	 */
	public void keyPressed(KeyEvent e){
		int keyCode = e.getKeyCode();
		if (keyCode == ks.getUp()) {
			entity.changeMovementState(Entity.MOVE_UP);
		} else if (keyCode == ks.getDown()) {
			entity.changeMovementState(Entity.MOVE_DOWN);
		} else if (keyCode == ks.getLeft()) {
			entity.changeMovementState(Entity.MOVE_LEFT);
		} else if (keyCode == ks.getRight()) {
			entity.changeMovementState(Entity.MOVE_RIGHT);
		} 
		e.consume();
	}

	/**
	 * Implementation of the KeyListener method
	 */
	public void keyReleased(KeyEvent e) {
		int keyCode = e.getKeyCode();
		if (keyCode == ks.getUp()) {
			entity.removeMovementState(Entity.MOVE_UP);
		} else if (keyCode == ks.getDown()) {
			entity.removeMovementState(Entity.MOVE_DOWN);
		} else if (keyCode == ks.getLeft()) {
			entity.removeMovementState(Entity.MOVE_LEFT);
		} else if (keyCode == ks.getRight()) {
			entity.removeMovementState(Entity.MOVE_RIGHT);
		}
		e.consume();
	}	
	
	/**
	 * Implementation of the KeyListener method
	 */
	@Override
	public void keyTyped(KeyEvent e) {

	}
	
}
