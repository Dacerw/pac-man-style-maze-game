import java.util.ArrayList;
import java.util.Random;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * This class is responsible for managing the state of the game, as well
 * as allowing for different parts of the application to interact.
 */
public class Game implements Runnable {
	
	private static Window window;
	
	private Thread thread;
	public boolean isDisplaying;
	
	private Maze maze;
	
	private DrawState drawState;
	private boolean settingsWindowOpen = false;
	
	//use by other classes to check if anything is currently being rendered
	private boolean isDisplayingGraphics = false;
	private DrawSettings drawSettings;

	private GameSettings settings;
	
	private List<Entity> entities;
	private List<Player> players;
	
	GameLogic gameLogic;
	
	private GameOptions gameOptions;
	
	public static enum GAME_STATE{
		MENU,
		GAME,
		GAME_MAZE,
		GAME_OVER,
		SETTINGS,
		EXIT,
	};
	
	private GAME_STATE state;
	
	public Game() {
		settings = new GameSettings();
		isDisplaying = true;
		window = new Window(settings.getResolutionWidth(), settings.getResolutionHeight(), settings.getIsFullscreen());
		drawState = new DrawMenu(this);
		this.state = GAME_STATE.MENU;
		this.launch();
	}
	
	/**
	 * Launches the thread for this game.
	 */
	private void launch() {
		thread = new Thread(this, "Game");
		thread.start();
	}
	
	/**
	 * Implements the Runnable method.
	 * Simply constantly renders the current screen, if it is showing.
	 */
	public void run() {
		while (true) {
			//cap number of frames at 100 fps
			if (!this.getWindow().isShowing()) continue;
			try {
				thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if (!isDisplaying) continue;
			if (window == null || !window.isShowing()) continue;
			isDisplayingGraphics = true;
			drawState.render();
			isDisplayingGraphics = false;
		}
	}
	
	/**
	 * Given a GAME_STATE, changes the state of the game to it.
	 * @param state a GAME_STATE containing the state to change into.
	 */
	public void changeState(GAME_STATE state) {
		switch (state) {
		case MENU:
			drawState.dispose();
			drawState = new DrawMenu(this);
			this.state = state;
			break;
		case GAME:
			drawState.dispose();
			gameOptions = new GameOptions();
			drawState = new DrawGameOptions(this);
			this.state = state;
			break;
		case GAME_MAZE:
			this.isDisplaying = false;
			drawState.dispose();
			createNewGame();
			this.isDisplaying = true;
			this.state = state;
			break;
		case GAME_OVER:
			drawState.dispose();
			drawState = new DrawEndScreen(this);
			this.state = state;
			break;
		case SETTINGS:
			if (settingsWindowOpen) break;
			settingsWindowOpen = true;
			drawSettings = new DrawSettings(this);
			break;
		case EXIT:
			System.exit(0);
			break;
		default:
			break;
		}
	}
	
	/**
	 * Given a resulting state, changes the state of the game to it.
	 * @param state a String containing the state to change into.
	 */
	public void changeState(String state) {
		if (state.equalsIgnoreCase("MENU")) {
			changeState(GAME_STATE.MENU);
		} else if (state.equalsIgnoreCase("GAME")) {
			changeState(GAME_STATE.GAME);
		} else if (state.equalsIgnoreCase("GAME_MAZE")) {
			changeState(GAME_STATE.GAME_MAZE);
		} else if (state.equalsIgnoreCase("SETTINGS")) {
			changeState(GAME_STATE.SETTINGS);
		} else if (state.equalsIgnoreCase("EXIT")) {
			changeState(GAME_STATE.EXIT);
		} else if (state.equalsIgnoreCase("GAME_OVER")) {
			changeState(GAME_STATE.GAME_OVER);
		}
	}
	
	/**
	 * Fetches the Maze object.
	 * @return a Maze object if it exists, null otherwise.
	 */
	public Maze getMaze() {
		return maze;
	}
	
	/**
	 * Fetches the width of the window.
	 * @return an integer containing the width.
	 */
	public int getWindowWidth() {
		return window.getWidth();
	}
	
	/**
	 * Fetches the height of the window.
	 * @return an integer containing the height.
	 */
	public int getWindowHeight() {
		return window.getHeight();
	}
	
	/**
	 * Fetches the window object which is currently displaying.
	 * @return a Window object.
	 */
	public Window getWindow() {
		return window;
	}
	
	/**
	 * Fetches the settings for this application.
	 * @return an object implementing the Settings interface.
	 */
	public Settings getSettings() {
		return settings;
	}
	
	/**
	 * Fetches the number of players in the game.
	 * @return an integer containing the number of players.
	 */
	public int getNumPlayers() {
		return this.players.size();
	}
	
	/**
	 * Fetches the Player object for a given player number.
	 * @param num an integer containing the player number.
	 * @return the corresponding Player object.
	 */
	public Player getPlayer(int num) {
		return this.players.get(num-1);
	}
	
	/**
	 * Fetches the GameOptions object.
	 * @return a GameOptions object.
	 */
	public GameOptions getGameOptions() {
		return this.gameOptions;
	}

	/**
	 * Fetches the object implementing the DrawState interface which is currently rendering.
	 * @return an object implementing the DrawState interface.
	 */
	public DrawState getDrawState() {
		return this.drawState;
	}
	
	/**
	 * Creates a new game.
	 */
	private void createNewGame() {
		int mazeWidth = gameOptions.getMazeWidth();
		int mazeHeight = gameOptions.getMazeHeight();
		maze = new Maze(mazeWidth, mazeHeight);
		drawState = new DrawMaze(this);
		entities = new CopyOnWriteArrayList<Entity>();
		createPlayers(gameOptions.getNumPlayers());
		createTreasure(gameOptions.getNumTreasures());
		gameLogic = new GameLogic(this);
	}
	
	/**
	 * Creates players for an instance of a game.
	 * @param numPlayers an integer containing the number of players.
	 */
	private void createPlayers(int numPlayers) {
		players = new CopyOnWriteArrayList<Player>();
		for (int i = 0;i< numPlayers;i++) {
			Player player = new Player(i,gameOptions.getStartingScore(),0);
			entities.add(player);
			players.add(player);
			assert(drawState instanceof DrawMaze);
			((DrawMaze)drawState).addKeyListener(new KeyInput(player, settings.getKeyboardSettings("Player"+(i+1))));
		}
	}
	
	/**
	 * Creates treasure for an instance of a game.
	 * @param numTreasures an integer containing the number of treasures to be created.
	 */
	private void createTreasure(int numTreasures) {
		//generate top left quadrant
		for (int i = 0; i < numTreasures/4; i++) {
			Entity treasure = new Treasure(getRandom(0, maze.getWidth()/2), getRandom(0, maze.getHeight()/2));
			entities.add(treasure);
		}
		//generate top right quadrant
		for (int i = 0; i < numTreasures/4; i++) {
			Entity treasure = new Treasure(getRandom(maze.getWidth()/2, maze.getWidth()-1), getRandom(0, maze.getHeight()/2));
			entities.add(treasure);
		}
		//generate bottom left quadrant
		for (int i = 0; i < numTreasures/4; i++) {
			Entity treasure = new Treasure(getRandom(0, maze.getWidth()/2), getRandom(maze.getHeight()/2, maze.getHeight()-1));
			entities.add(treasure);
		}
		//generate bottom right quadrant
		for (int i = 0; i < numTreasures/4; i++) {
			Entity treasure = new Treasure(getRandom(maze.getWidth()/2, maze.getWidth()-1), getRandom(maze.getHeight()/2, maze.getHeight()-1));
			entities.add(treasure);
		}
	}
	
	/**
	 * Returns a random position within a maze, given a minimum, and maximum tile pos.
	 * @param min an integer containing the min tile pos.
	 * @param max an integer containing the max tile pos.
	 * @return an integer containing the position within the maze.
	 */
	private int getRandom(int min, int max) {
		Random generator = new Random();
		int rnd = Math.abs(generator.nextInt()) % (max-min) + min;
		return (rnd*Entity.TILE_SIZE+Entity.TILE_SIZE/2);
	}
	
	/**
	 * Fetches all the entities in the game.
	 * @return a List\<Entity\> object containing the entities.
	 */
	public List<Entity> getEntities() {
		return entities;
	}
	
	/**
	 * Removes a given entity from the game.
	 * @param e an Entity object containing the entity to be removed.
	 */
	public void removeEntity(Entity e) {
		entities.remove(e);
	}
	
	/**
	 * Sets the settings window to be either open or closed.
	 * @param value a boolean value containing true if open, false if closed.
	 */
	public void setSettingsWindowOpen(boolean value) {
		settingsWindowOpen = value;
	}
	
	/**
	 * Determines if the graphics are currently being displayed.
	 * @return a boolean containing true if displaying, false otherwise.
	 */
	public boolean getIsDisplayingGraphics() {
		return isDisplayingGraphics;
	}
	
	/**
	 * Sets the cursor of the user to a hand.
	 */
	public void setCursorToHand(){
		getWindow().setCursorToHand();
	}
	
	/**
	 * Sets the cursor of the user to their default cursor.
	 */
	public void setCursorToDefault(){
		getWindow().setCursorToDefault();
	}
	
	public static void main(String[] args) {
		new Game();
	}
	
}
