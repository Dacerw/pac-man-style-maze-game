
/**
 * This class contains information about a specific instance of a game.
 */
public class GameOptions {
	
	public static final int EASY_DIFFICULTY = 0;
	public static final int MEDIUM_DIFFICULTY = 1;
	public static final int HARD_DIFFICULTY = 2;
	
	private int numPlayers;
	private int difficulty;
	
	public GameOptions() {
		this.numPlayers = 1;
		this.difficulty = MEDIUM_DIFFICULTY;
	}
	
	/**
	 * Determines the number of players in the game.
	 * @return an integer containing the number of players.
	 */
	public int getNumPlayers() {
		return numPlayers;
	}
	
	/**
	 * Determines the difficulty of the game.
	 * @return an integer representing the difficulty.
	 */
	public int getDifficulty() {
		return difficulty;
	}
	
	/**
	 * Sets the difficulty of the game.
	 * @param difficulty an integer representing the difficulty.
	 */
	public void setDifficulty(int difficulty) {
		this.difficulty = difficulty; 
	}
	
	/**
	 * Sets the number of players in the game.
	 * @param numPlayers an integer containing the number of players.
	 */
	public void setNumPlayers(int numPlayers) {
		this.numPlayers = numPlayers;
	}

	/**
	 * Determines the width of the maze, based on its difficulty.
	 * @return an integer containing the width.
	 */
	public int getMazeWidth() {
		if (difficulty == EASY_DIFFICULTY) {
			return 10;
		} else if (difficulty == MEDIUM_DIFFICULTY) {
			return 20;
		} else if (difficulty == HARD_DIFFICULTY) {
			return 30;
		}
		assert(false);
		return 0;
	}
	
	/**
	 * Determines the height of the maze, based on its difficulty.
	 * @return an integer containing the height.
	 */
	public int getMazeHeight() {
		if (difficulty == EASY_DIFFICULTY) {
			return 10;
		} else if (difficulty == MEDIUM_DIFFICULTY) {
			return 20;
		} else if (difficulty == HARD_DIFFICULTY) {
			return 30;
		}
		assert(false);
		return 0;
	}
	
	/**
	 * Determines the number of treasures in the maze, based on its difficulty.
	 * @return an integer containing the number of treasures..
	 */
	public int getNumTreasures() {
		if (difficulty == EASY_DIFFICULTY) {
			return 4;
		} else if (difficulty == MEDIUM_DIFFICULTY) {
			return 8;
		} else if (difficulty == HARD_DIFFICULTY) {
			return 20;
		}
		assert(false);
		return 0;
	}
	
	/**
	 * Determines the player's starting score, based on the maze's difficulty.
	 * @return an integer containing the starting score.
	 */
	public int getStartingScore() {
		if (difficulty == EASY_DIFFICULTY) {
			return 200;
		} else if (difficulty == MEDIUM_DIFFICULTY) {
			return 400;
		} else if (difficulty == HARD_DIFFICULTY) {
			return 800;
		}
		assert(false);
		return 0;
	}
}
