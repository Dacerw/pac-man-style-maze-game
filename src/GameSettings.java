import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * This class is used to load, manage, and store settings for this application.
 */
public class GameSettings implements Settings {
	
	public static final String settingsFile = "resources/settings.txt";
	
	private boolean isFullscreen;
	private int width;
	private int height;
	private KeyboardSettings playerOneKeys;
	private KeyboardSettings playerTwoKeys;
	private String graphicsQuality;
	
	//for demonstration only
	private boolean scoreCountdown;

	public GameSettings() {
		loadSettings();
	}

	/**
	 * Implements the Settings method.
	 */
	public void loadSettings() {
		try {
			Scanner inputFile = new Scanner(new FileReader(settingsFile));
			String[] lineInput;

			while (inputFile.hasNextLine()) {
				// Read in a whole line
				String line = inputFile.nextLine();
				lineInput = line.split(" ");

				// Fullscreen: [true/false]
				if (lineInput[0].equals("Fullscreen:")) {
					String isFullscreen = lineInput[1];
					if (isFullscreen.equals("true")) {
						setFullscreen(true);
					} else if (isFullscreen.equals("false")) {
						setFullscreen(false);
					}
					
					// Resolution: [Width] x [Height]
				} else if (lineInput[0].equals("Resolution:")) {
					int width = Integer.parseInt(lineInput[1]);
					int height = Integer.parseInt(lineInput[3]);
					setResolutionWidth(width);
					setResolutionHeight(height);

					// Keyboard: [Player1/Player2] [UP KEY] [DOWN KEY] [LEFT
					// KEY] [RIGHT KEY]
				} else if (lineInput[0].equals("Keyboard:")) {
					String player = lineInput[1];
					String up = lineInput[2];
					String down = lineInput[3];
					String left = lineInput[4];
					String right = lineInput[5];

					if (player.equals("Player1")) {
						playerOneKeys = new KeyboardSettings(up, down, left, right);
					} else if (player.equals("Player2")) {
						playerTwoKeys = new KeyboardSettings(up, down, left, right);
					}
				} else if (lineInput[0].equals("Graphics_quality:")) {
					String quality = lineInput[1];
					setGraphicsQuality(quality);
				} else if (lineInput[0].equals("Score_countdown:")) {
					String isScoreCountdown = lineInput[1];
					if (isScoreCountdown.equals("true")) {
						setScoreCountdown(true);
					} else if (isScoreCountdown.equals("false")) {
						setScoreCountdown(false);
					}
				}
			}
			inputFile.close();
		} catch (FileNotFoundException e) {
			System.out.println("File is not found.");
		}
	}

	/**
	 * Implements the Settings method.
	 */
	public int getResolutionWidth() {
		return this.width;
	}

	/**
	 * Implements the Settings method.
	 */
	public int getResolutionHeight() {
		return this.height;
	}

	/**
	 * Implements the Settings method.
	 */
	public boolean getIsFullscreen() {
		return this.isFullscreen;
	}
	
	/**
	 * Implements the Settings method.
	 */
	public boolean getScoreCountdown() {
		return this.scoreCountdown;
	}

	/**
	 * Implements the Settings method.
	 */
	public KeyboardSettings getKeyboardSettings(String player){
		if (player.equals("Player1")){
			return playerOneKeys;
		} else if  (player.equals("Player2")){
			return playerTwoKeys;
		} else {
			return null;
		}
	}
	
	/**
	 * Implements the Settings method.
	 */
	public void setResolutionWidth(int inputWidth) {
		this.width = inputWidth;
	}

	/**
	 * Implements the Settings method.
	 */
	public void setResolutionHeight(int inputHeight) {
		this.height = inputHeight;
	}

	/**
	 * Implements the Settings method.
	 */
	public void setFullscreen(boolean inputIsFullScreen) {
		this.isFullscreen = inputIsFullScreen;

	}
	
	/**
	 * Implements the Settings method.
	 */
	public void setScoreCountdown(boolean scoreCountdown) {
		this.scoreCountdown = scoreCountdown;
	}

	/**
	 * Implements the Settings method.
	 */
	public void setKeyboardSettings(String player, String up, String down,
			String left, String right) {
		if (player.equals("Player1")){
			playerOneKeys.setKeyboardSettings(up, down, left, right);
		} else if  (player.equals("Player2")){
			playerTwoKeys.setKeyboardSettings(up, down, left, right);
		}
	}

	/**
	 * Implements the Settings method.
	 */
	public void writeSettings() {
		BufferedWriter bufferedWriter = null;
        try {

            bufferedWriter = new BufferedWriter(new FileWriter(settingsFile));
            
            bufferedWriter.write("Fullscreen: " + String.valueOf(isFullscreen));
            bufferedWriter.newLine();
            bufferedWriter.write("Resolution: " + Integer.toString(width) + " x " + Integer.toString(height));
            bufferedWriter.newLine();
            bufferedWriter.write("Keyboard: Player1 " + playerOneKeys.getKeyboardSettings());
            bufferedWriter.newLine();
            bufferedWriter.write("Keyboard: Player2 " + playerTwoKeys.getKeyboardSettings());
            bufferedWriter.newLine();
            bufferedWriter.write("Graphics_quality: " + graphicsQuality);
            bufferedWriter.newLine();
            bufferedWriter.write("Score_countdown: " + String.valueOf(scoreCountdown));
            
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (bufferedWriter != null) {
                    bufferedWriter.flush();
                    bufferedWriter.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

	}

	/**
	 * Implements the Settings method.
	 */
	@Override
	public String getGraphicsQuality() {
		return graphicsQuality;
	}

	/**
	 * Implements the Settings method.
	 */
	@Override
	public void setGraphicsQuality(String quality) {
		this.graphicsQuality = quality;
	}

}
