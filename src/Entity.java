import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * An implementation of a game object which contains a location, graphics,
 * and may also move.
 */
public abstract class Entity implements GameObject {
	public static final int NOT_MOVING = 0;
	public static final int MOVE_UP = 4;
	public static final int MOVE_DOWN = 1;
	public static final int MOVE_LEFT = 2;
	public static final int MOVE_RIGHT = 3;
	public static final double PLAYER_RATIO_X = 0.52; //these ratio were measured based on the size of the graphics
	public static final double PLAYER_RATIO_Y = 0.64;
	public static final double WALL_RATIO = 0.17;
	public static final int TILE_SIZE = 100;
	private static final int TREASURE_ENTITY = 10;
	private static final String characterImagesPath = new String("resources/sprite assets");
	private static final String treasureImagePath = new String("resources/treasure assets/Coin-icon.png");
	
	private HashMap<Integer,String> charFilePaths = new HashMap<Integer,String>();
	
	//The ArrayList contains the order in which movement keys have been pressed,
	//with the most recent being in index 0.
	//If empty, no movement keys are being pressed.
	private LinkedBlockingDeque<Integer> movementState = new LinkedBlockingDeque<Integer>();
	//private int movementState;
	private double xPos;
	private double yPos;
	private int entityType;
	private double speed;
	private int runningState;

	
	public Entity(double setX, double setY, double speed, int entity) {
	//	movementState = NOT_MOVING;
		xPos = setX;
		yPos = setY;
		entityType = entity;
		this.speed = speed;
		runningState = entityType*12;
		loadCharPaths();
	}
	
	/**
	 * This would be determined by game according to how big the map is
	 * and how fast the default play speed it.
	 * @param newSpeed determined by Game.
	 */
	public void changeXSpeed(int newSpeed) {
		speed = newSpeed;
	}
	
	/**
	 * Determines if the entity is moving and in what direction.
	 * @param newState this would be set by KeyInput based on
	 * user input.
	 */
	public void changeMovementState(int newState){
		movementState.removeAll(Collections.singleton(newState));
		movementState.addFirst(newState);
	}
	
	public void removeMovementState(int state){
		movementState.removeAll(Collections.singleton(state));
	}
	
	/**
	 * This function is called periodically by the game in order to
	 * move the entity in whatever direction it is meant to be going
	 * as determined by the keyInput class.
	 * @param1 is the time since this function was last called. This variable
	 * ensures that even if there is a delay in calling this function, the movement
	 * of the character occurs at a constant rate.
	 * timeSinceLast moved should in seconds, hence if speed is set to 100
	 * the entity will move one tile a second
	 */
	public synchronized void moveEntity(double timeSinceLastMove, Game game) {
		double changeValue = speed*timeSinceLastMove;
		Maze maze = game.getMaze();
		
		if (movementState.size() == 0) return;
		switch (movementState.getFirst()) {
		case NOT_MOVING:
			break;
		case MOVE_UP:
			if (!checkForCollision(xPos,yPos-changeValue, maze)) {
				yPos = yPos - changeValue;
			} else {
				collisionEvent();
			}
			break;
		case MOVE_DOWN:
			if (!checkForCollision(xPos,yPos+changeValue,maze)) {
				yPos = yPos + changeValue;
			} else {
				collisionEvent();
			}
			break;
		case MOVE_LEFT:
			if (!checkForCollision(xPos-changeValue,yPos,maze)) {
				xPos = xPos - changeValue;
			} else {
				collisionEvent();
			}
			break;
		case MOVE_RIGHT:
			if (!checkForCollision(xPos+changeValue,yPos,maze)) {
				xPos = xPos + changeValue;
			} else {
				collisionEvent();
			}
			break;
		default:
			System.out.println("Entity Class: Invalid movement state detected.");
			break;
		}
	}
	
	/**
	 * @return the current position of the entity in the x axis.
	 */
	public double getXPos() {
		return xPos;
	}
	
	/**
	 * @return the current position of the entity in the y axis.
	 */
	public double getYPos() {
		return yPos;
	}
	
	/**
	 * Sets X pos
	 * @param xPos a double containing the value
	 */
	public void setXPos(double xPos) {
		this.xPos = xPos;
	}
	
	/**
	 * Sets Y pos
	 * @param yPos a double containing the value
	 */
	public void setYPos(double yPos) {
		this.yPos = yPos;
	}
	
	/**
	 * @return the type of entity that is being displayed, would correspond
	 * with a set of images and attributes to display that character.
	 */
	public int getEntityType() {
		return entityType;
	}
	
	/**
	 * @return returns the current speed of the entity, which is measured
	 * in how many grid positions the entity moves per cycle (when the
	 * moveEntity method is called).
	 */
	public double getSpeed() {
		return speed;
	}
	
	/**
	 * In order to display different character images to simulate walking etc
	 * a method is needed to return different images depending on if the
	 * character is moving or not and at what stage of walking it is at.
	 * @return an integer that represents a stage of the walking cycle.
	 * NOT_MOVING returns the last set runningState
	 * MOVE_UP returns an integer from 10-19
	 * MOVE_DOWN returns an integer from 20-29
	 * MOVE_LEFT returns an integer from 30-39
	 * MOVE_RIGHT returns an integer from 40-49
	 */
	public String getRunningState() {
		if (entityType == TREASURE_ENTITY) return treasureImagePath;
		if (movementState.size() == 0) return charFilePaths.get(runningState);
		switch (movementState.getFirst()) {
		case NOT_MOVING:
			break;
		case MOVE_UP:
			runningState = (int) (yPos) % 3;
			runningState = runningState + (MOVE_UP-1)*3 + entityType*12;
			break;
		case MOVE_DOWN:
			runningState = (int) (yPos) % 3;
			runningState = runningState + (MOVE_DOWN-1)*3 + entityType*12;
			break;
		case MOVE_LEFT:
			runningState = (int) (xPos) % 3;
			runningState = runningState + (MOVE_LEFT-1)*3 + entityType*12;
			break;
		case MOVE_RIGHT:
			runningState = (int) (xPos) % 3;
			runningState = runningState + (MOVE_RIGHT-1)*3 + entityType*12;
			break;
		default:
			System.out.println("Entity Class: Invalid movement state detected.");
			break;
		}
		return charFilePaths.get(runningState);
	}
	
	/**
	 * Checks if at the current position, there is a collision with terrain.
	 * @param x potential x grid position of the entity
	 * @param y potential y grid position of the entity
	 * @return boolean value, true if there is a collision, otherwise false.
	 */
	private boolean checkForCollision(double x, double y, Maze maze) {
		return maze.collides(x, y, WALL_RATIO, getSizeRatioX(), getSizeRatioY(), TILE_SIZE);
	}
	
	/**
	 * Potential method that causes an event to occur when there
	 * is a collision detected.
	 */
	private void collisionEvent() {
		//something happens when a wall collision is detected,
		//maybe a sound or special character image with hands in air.
	}
	
	/**
	 * Loads the character image file paths into a Hashmap
	 * to be used by the runningState method.
	 */
	private void loadCharPaths() {
		File folder = new File(characterImagesPath);
		File[] listOfFiles = folder.listFiles();
		Arrays.sort(listOfFiles);
		int i = 0;
		for (File f : listOfFiles) {
			String pathToFile = f.getName();
			Integer charCode = i;
			charFilePaths.put(charCode, new String(characterImagesPath + "/" + pathToFile));
			i++;
		}
	}
	
	public abstract double getSizeRatioX();
	
	public abstract double getSizeRatioY();
}
