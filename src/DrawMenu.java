import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;

/**
 * This class is responsible for drawing the menu screen when the program is run.
 * It is also responsible for adding a mouse listener for this class to the game's window.
 */
public class DrawMenu implements DrawState {
	private static final String menuAssetsPath = new String("resources/menu assets");
	
	private int wWidth;
	private int wHeight;
	
	public Game game;
	private DrawMenuInput ml;
	
	private RectangleComponent newGame;
	private RectangleComponent quit;
	private RectangleComponent settings;
	
	public BufferedImage curMenuState;
	public BufferedImage menuBackgroundDefault;
	public BufferedImage menuBackgroundNewGame;
	public BufferedImage menuBackgroundQuit;
	public BufferedImage menuBackgroundSettings;
	
	public DrawMenu(Game g) {
		this.game = g;
		loadMenuAssets();
		curMenuState = menuBackgroundDefault;
		
		wWidth = g.getWindowWidth();
		wHeight = g.getWindowHeight();
		
		newGame = new RectangleComponent(wWidth/13, wHeight/20, wWidth/3, wHeight/11);
		settings = new RectangleComponent(wWidth/13, wHeight*7/38, wWidth/3, wHeight/11);
		quit = new RectangleComponent(wWidth/13, wHeight*6/19, wWidth/3, wHeight/11);
	
		ml = new DrawMenuInput(this, newGame, quit, settings);
		game.getWindow().addMouseListener(ml);
		game.getWindow().addMouseMotionListener(ml);
	}
	
	/**
	 * Displays graphics for the DrawMenu screen
	 */
	@Override
	public void render() {
		Window window = game.getWindow();
		wWidth = window.getWidth();
		wHeight = window.getHeight();
		BufferStrategy bs = window.getBufferStrategy();
		Graphics g = bs.getDrawGraphics();
		g.fillRect(0,0,window.getWidth(),window.getHeight());
		
		//Updates the location/size of the buttons
		newGame.updateLocation(wWidth/13, wHeight/20);
		newGame.updateSize(wWidth/3, wHeight/11);
		settings.updateLocation(wWidth/13, wHeight*7/38);
		settings.updateSize(wWidth/3, wHeight/11);
		quit.updateLocation(wWidth/13, wHeight*6/19);
		quit.updateSize(wWidth/3, wHeight/11);
		
		g.drawImage(curMenuState, 0, 0, wWidth, wHeight, null);
		
		g.dispose();
		bs.show();
	}
	
	/**
	 * Destroys the DrawMenu's mouse listener resources
	 */
	@Override
	public void dispose() {
		game.getWindow().removeMouseListener(ml);
		game.getWindow().removeMouseMotionListener(ml);
	}
	
	/**
	 * Loads background image assets from its file path
	 */
	private void loadMenuAssets() {
		menuBackgroundDefault = GraphicsUtilities.loadImage(menuAssetsPath + "/homescreen-default.jpg");
		menuBackgroundNewGame = GraphicsUtilities.loadImage(menuAssetsPath + "/homescreen-newgame.jpg");
		menuBackgroundQuit = GraphicsUtilities.loadImage(menuAssetsPath + "/homescreen-quit.jpg");
		menuBackgroundSettings = GraphicsUtilities.loadImage(menuAssetsPath + "/homescreen-settings.jpg");
	}
}
