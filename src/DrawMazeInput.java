import java.awt.Component;
import java.awt.Cursor;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

/**
 * This class is responsible for dealing with the consequences of 
 * specific mouse movements/actions for the actual game screen (DrawMenu class).
 */
public class DrawMazeInput extends DrawStateInput {

	private DrawMaze dm;
	
	private Component settings;
	private Component quit;
	
	private static final int NONE = 0;
	private static final int SETTINGS_BUTTON = 1;
	private static final int QUIT_BUTTON = 2;
	
	private int lastButtonPressed = NONE;
	
	public DrawMazeInput(DrawMaze dm, Component settings, Component quit) {
		this.dm = dm;
		this.settings = settings;
		this.quit = quit;
	}

	/**
	 * Changes the cursor to a hand if its over a button.
	 * Changes the cursor to a default pointer if its over a button.
	 */
	@Override
	public void mouseMoved(MouseEvent e) {
		int mouseX = e.getX();
		int mouseY = e.getY();
		if (mouseOver(settings, mouseX, mouseY) || mouseOver(quit, mouseX, mouseY)){
			dm.game.setCursorToHand();
		} else {
			dm.game.setCursorToDefault();
		}
		e.consume();
	}

	/**
	 * Changes the background based on the button that is pressed.
	 */
	@Override
	public void mousePressed(MouseEvent e) {
		int mouseX = e.getX();
		int mouseY = e.getY();
		if (dm.game.getGameOptions().getNumPlayers() == 1){
			if (mouseOver(settings, mouseX, mouseY)) {
				dm.currPlayerInfoState = dm.singleClickSettings;
				lastButtonPressed = SETTINGS_BUTTON;
			} else if (mouseOver(quit, mouseX, mouseY)) {
				dm.currPlayerInfoState = dm.singleClickQuit;
				lastButtonPressed = QUIT_BUTTON;
			} else {
				dm.currPlayerInfoState = dm.singleInfo;
			}
		} else {
			if (mouseOver(settings, mouseX, mouseY)) {
				dm.currPlayerInfoState = dm.multiClickSettings;
				lastButtonPressed = SETTINGS_BUTTON;
			} else if (mouseOver(quit, mouseX, mouseY)) {
				dm.currPlayerInfoState = dm.multiClickQuit;
				lastButtonPressed = QUIT_BUTTON;
			} else {
				dm.currPlayerInfoState = dm.multiInfo;
			}
		}
	}
	
	/**
	 * Changes the background based on the button that is released. 
	 */
	@Override
	public void mouseReleased(MouseEvent e) {
		int mouseX = e.getX();
		int mouseY = e.getY();
		if (mouseOver(settings, mouseX, mouseY) && lastButtonPressed == SETTINGS_BUTTON) {
			dm.game.changeState("SETTINGS");
		} else if (mouseOver(quit, mouseX, mouseY) && lastButtonPressed == QUIT_BUTTON) {
			dm.game.changeState("MENU");
		} else {
			dm.game.setCursorToDefault();
		}
		dm.currPlayerInfoState = (dm.game.getGameOptions().getNumPlayers() == 1) ? dm.singleInfo : dm.multiInfo;
		lastButtonPressed = NONE;
	}

	/**
	 * Implements the interface method but does nothing.
	 */
	@Override
	public void mouseEntered(MouseEvent e) {
	}

	/**
	 * Implements the interface method but does nothing.
	 */
	@Override
	public void mouseExited(MouseEvent e) {
	}

	/**
	 * Implements the interface method but does nothing.
	 */
	@Override
	public void mouseClicked(MouseEvent e) {
	}

	/**
	 * Implements the interface method but does nothing.
	 */
	@Override
	public void mouseDragged(MouseEvent e) {
	}

	/**
	 * Checks if a mouse is over a specified component 
	 */
	public boolean mouseOver(Component c, int mouseX, int mouseY) {
		return (c.getLocation().x <= mouseX && c.getLocation().x + c.getWidth() >= mouseX &&
			c.getLocation().y <= mouseY && c.getLocation().y + c.getHeight() >= mouseY);
	}
}
