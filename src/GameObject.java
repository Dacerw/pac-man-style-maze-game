
public interface GameObject {
	
	/**
	 * This would be determined by game according to how big the map is
	 * and how fast the default play speed it.
	 * @param newSpeed determined by Game.
	 */
	public void changeXSpeed(int newSpeed);
	
	/**
	 * Determines if the entity is moving and in what direction.
	 * @param newState this would be set by KeyInput based on
	 * user input.
	 */
	public void changeMovementState(int newState);
	
	/**
	 * Clears the movement state of the movementState passed to.
	 * @param state
	 */
	public void removeMovementState(int state);
	
	/**
	 * This function is called periodically by the game in order to
	 * move the entity in whatever direction it is meant to be going
	 * as determined by the keyInput class.
	 * @param1 is the time since this function was last called. This variable
	 * ensures that even if there is a delay in calling this function, the movement
	 * of the character occurs at a constant rate.
	 * timeSinceLast moved should in seconds, hence if speed is set to 100
	 * the entity will move one tile a second
	 */
	public void moveEntity(double timeSinceLastMove, Game game);
	
	/**
	 * @return the current position of the entity in the x axis.
	 */
	public double getXPos();
	
	/**
	 * @return the current position of the entity in the y axis.
	 */
	public double getYPos();
	
	/**
	 * Sets X pos
	 * @param xPos a double containing the value
	 */
	public void setXPos(double xPos);
	
	/**
	 * Sets Y pos
	 * @param yPos a double containing the value
	 */
	public void setYPos(double yPos);
	
	/**
	 * @return the type of entity that is being displayed, would correspond
	 * with a set of images and attributes to display that character.
	 */
	public int getEntityType();
	
	/**
	 * @return returns the current speed of the entity, which is measured
	 * in how many grid positions the entity moves per cycle (when the
	 * moveEntity method is called).
	 */
	public double getSpeed();
	
	/**
	 * In order to display different character images to simulate walking etc
	 * a method is needed to return different images depending on if the
	 * character is moving or not and at what stage of walking it is at.
	 * @return an integer that represents a stage of the walking cycle.
	 * NOT_MOVING returns the last set runningState
	 * MOVE_UP returns an integer from 10-19
	 * MOVE_DOWN returns an integer from 20-29
	 * MOVE_LEFT returns an integer from 30-39
	 * MOVE_RIGHT returns an integer from 40-49
	 */
	public String getRunningState();
}
