import java.util.ArrayList;
import java.util.List;

/**
 * This class is responsible for handling features within a maze. It moves players,
 * checks for collision, and checks for when the game has ended.
 */
public class GameLogic implements Runnable {
	
	//per second
	public static final double SCORE_DECAY = 10.0;
	
	//extra score awarded to the player who finished first
	public static final int FINISH_SCORE = 200;
	
	private Game game;
	
	Timer tEntity;
	Timer tScore;
	Timer tTreasure;
	
	Thread thread;
	
	boolean isRunning;
	int hasPlayerOneWon;
	
	List<Player> players;
	List<Treasure> treasures;
	
	public static final double TREASURE_ABOVE_PLAYER_TIME = 1.0;
	ArrayList<TreasurePlayerTimer> treasureOnPlayer = new ArrayList<TreasurePlayerTimer>();
	
	public GameLogic(Game game) {
		this.game = game;
		tEntity = new Timer();
		tEntity.start();
		
		tScore = new Timer();
		tScore.start();
		
		tTreasure = new Timer();
		tTreasure.start();
		
		isRunning = true;
		thread = new Thread(this, "Game logic");
		thread.start();
	}	
	
	/**
	 * Runs through the list of players and treasures and checks if there are any
	 * collisions between the two.
	 */
	private void checkForTreasureFound() {
		for (Player p : players) {
			for (Treasure t : treasures) {
				if (checkIfClose(p.getXPos(),p.getYPos(),t.getXPos(),t.getYPos()) && 
					!treasureOnPlayer(t)) {
					treasureOnPlayer.add(new TreasurePlayerTimer(p, t));
					p.changeScore(true, 100);
				}
			}
		}
	}
	
	/**
	 * Checks if a given treasure is above a player's head.
	 * @param t a Treasure object to check.
	 * @return a boolean value containing true if the given treasure is above any player's head,
	 * and false otherwise.
	 */
	private boolean treasureOnPlayer(Treasure t) {
		for (TreasurePlayerTimer tpt : treasureOnPlayer) {
			if (tpt.getTreasure().equals(t)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * For each treasure currently above a player, updates its location, in case the player
	 * has moved, and makes it disappear if a second has passed.
	 */
	private void placeTreasureOnPlayer() {
		for (TreasurePlayerTimer tpt : treasureOnPlayer) {
			Treasure t = tpt.getTreasure();
			Player p = tpt.getPlayer();
			t.setXPos(p.getXPos());
			t.setYPos(p.getYPos()-p.getSizeRatioY()*Entity.TILE_SIZE);
			tpt.setTimeLeft(tpt.getTimeLeft()-tTreasure.getTimeS());
			if (tpt.getTimeLeft() < 0) {
				game.removeEntity(t);
			}
		}
	}
	
	/**
	 * Checks if two given entities are within half a tile space of each other.
	 */
	private boolean checkIfClose(double x1, double y1, double x2, double y2) {
		if ((Math.abs(x1-x2) < Entity.TILE_SIZE/2) && (Math.abs(y1-y2) < Entity.TILE_SIZE/2)) return true;
		return false;
	}
	
	/**
	 * Returns a list of entities contain all the players
	 */
	private List<Player> getPlayers(List<Entity> entities) {
		List<Player> newList = new ArrayList<Player>();
		for (Entity e : entities) {
			if (e instanceof Player) newList.add((Player)e);
		}
		return newList;
	}
	
	/**
	 * Returns a list of entities containing all the treasures.
	 */
	private List<Treasure> getTreasures(List<Entity> entities) {
		List<Treasure> newList = new ArrayList<Treasure>();
		for (Entity e : entities) {
			if (e instanceof Treasure) newList.add((Treasure)e);
		}
		return newList;
	}
	
	/**
	 * Checks if the player is in a position outside the maze area and thus has found
	 * the exit to the maze
	 * @return true if the player has found the exit.
	 */
	private boolean playerFoundExit() {
		for (Player p : players) {
			if (p.getYPos() >= game.getMaze().getHeight()*Entity.TILE_SIZE) {
				if (players.size() > 1)
					p.changeScore(true, FINISH_SCORE);
				return true;
			}
		}
		return false;
	}

	/**
	 * Continuously runs, updating the state of the game.
	 */
	@Override
	public void run() {
		while (isRunning) {
			if (((DrawMaze)(game.getDrawState())).getIsLoadingScreen()) {
				tEntity.update();
				tScore.update();
				tTreasure.update();
				try {
					thread.sleep(1);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				continue;
			}
			List<Entity> entities = game.getEntities();
			if (entities == null) continue;
			//move entities
			if (entities != null) {
				for (Entity e : entities) {
					e.moveEntity(tEntity.getTimeS(), game);
				}
			}
			tEntity.update();
			players = getPlayers(entities);
			//decrease score if it should be decreased by 1
			if (tScore.getTimeS()*SCORE_DECAY >= 1.0 && game.getSettings().getScoreCountdown()) {
				for (Player p : players) {
					if (p.getScore() == 0) {
						hasPlayerOneWon();
						game.changeState("GAME_OVER");
						isRunning = false;
					} else {
						p.incDecScore(false);
					}
				}
				tScore.update();
			}
			if (playerFoundExit()) {
				hasPlayerOneWon();
				game.changeState("GAME_OVER");
				isRunning = false;
			}
			//Handle treasure collision
			treasures = getTreasures(entities);
			checkForTreasureFound();
			placeTreasureOnPlayer();
			tTreasure.update();
			try {
				thread.sleep(1);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	/**
	 * Checks if a player has won or not, or if in multiplayer, which player has won.
	 */
	private void hasPlayerOneWon(){
		if (players.size() == 1){
			if (players.get(0).getScore() > 0){
				hasPlayerOneWon = 1;
			} else {
				hasPlayerOneWon = -1;
			}
		} else {
			if (players.get(0).getScore() - players.get(1).getScore() > 0){
				hasPlayerOneWon = 1;
			} else if (players.get(0).getScore() - players.get(1).getScore() == 0){
				hasPlayerOneWon = 0;
			} else {
				hasPlayerOneWon = -1;
			}
		}
	}
	
	/**
	 * Used to place a treasure above a player's head, and check when it should disappear.
	 */
	private class TreasurePlayerTimer {
		
		double timeLeft;
		Player p;
		Treasure t;
		
		public TreasurePlayerTimer(Player p, Treasure t) {
			this.p = p;
			this.t = t;
			this.timeLeft = TREASURE_ABOVE_PLAYER_TIME; 
		}
		
		public Player getPlayer() {
			return p;
		}
		
		public Treasure getTreasure() {
			return t;
		}
		
		public double getTimeLeft() {
			return timeLeft;
		}
		
		public void setTimeLeft(double timeLeft) {
			this.timeLeft = timeLeft;
		}
	}
}