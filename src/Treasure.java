/**
 * Implementation of a Treasure entity in the maze.
 */
public class Treasure extends Entity {
	public static final int NOT_MOVING = 0;
	
	private static final double TREASURE_SPEED = 0;
	
	private static final int ENTITY_TYPE = 10;
	
	public Treasure(int setX, int setY) {
		super(setX, setY, TREASURE_SPEED, ENTITY_TYPE);
		changeMovementState(NOT_MOVING);
	}
	
	/**
	 * Returns the size of the treasure in the x-axis as a fraction of tile size (which is 1).
	 */
	@Override
	public double getSizeRatioX() {
		return 0.6;
	}
	
	/**
	 * Returns the size of the treasure in the y-axis as a fraction of tile size (which is 1).
	 */
	@Override
	public double getSizeRatioY() {
		return 0.6;
	}
	
}