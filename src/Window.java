import java.awt.Canvas;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Toolkit;

import javax.swing.JFrame;

/**
 * This class is responsible for creating a window that is called by the Game class
 * and used while the program is run.
 */
public class Window extends Canvas {
	
	private boolean fullscreen;
	private Dimension dimension;
	private JFrame frame;
		
	public Window(int width, int height, boolean fullscreen) {
		if (fullscreen) setFullscreen();
		else setDimension(width, height);
	}
	
	/**
	 * Sets up and loads the frame of a window, based on dimensions
	 * @param width an integer containing the width of the window.
	 * @param height an integer containing the height of the window.
	 */
	private void loadFrame(int width, int height) {
		frame.setResizable(false);
		dimension = new Dimension(width, height);
		this.setPreferredSize(dimension);
		this.setMaximumSize(dimension);
		this.setMinimumSize(dimension);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.frame.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
		this.frame.pack();
		this.frame.setLocationRelativeTo(null);
		
		this.frame.add(this);
		this.frame.pack();
		this.frame.setLocationRelativeTo(null);
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setFocusable(true);
		this.createBufferStrategy(3);
		launchWindow();
	}

	/**
	 * Launches window
	 */
	public void launchWindow() {
		showWindow();
		this.requestFocus();
	}

	/**
	 * Sets the window to be visible
	 */
	public void showWindow() {
		this.frame.setVisible(true);
	}
	
	/**
	 * Sets the window to be invisible
	 */
	public void hideWindow() {
		this.frame.setVisible(false);
	}

	/**
	 * Gets whether or not the window is set to full screen
	 * @return a boolean value containing the answer.
	 */
	public boolean getIsFullscreen() {
		return fullscreen;
	}
	
	/**
	 * Gets the window width from Dimension field
	 * @return an integer containing the width.
	 */
	public int getWindowWidth() {
		return (int) dimension.getWidth();
	}
	
	/**
	 * Gets the window height from Dimension field
	 * @return an integer containing the height.
	 */
	public int getWindowHeight() {
		return (int) dimension.getHeight();
	}
	
	/**
	 * Sets the window to fullScreen
	 */
	public void setFullscreen() {
		fullscreen = true;
		this.frame = new JFrame(new String("Labyrinth"));
		frame.setUndecorated(true);
		
		GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
		
		//Simulates fullScreen
		int width = gd.getDisplayMode().getWidth();
		int height = gd.getDisplayMode().getHeight();
	
		loadFrame(width, height);
	}

	/**
	 * Sets the window's dimensions based on a given width/height
	 * @param width an integer containing the width.
	 * @param height an integer containing the height.
	 */
	public void setDimension(int width, int height) {
		fullscreen = false;
		this.frame = new JFrame(new String("Labyrinth"));
		frame.setUndecorated(false);
		loadFrame(width, height);
	}

	/**
	 * Sets the cursor to a default pointer image
	 */
	public void setCursorToDefault(){
		Cursor defaultCursor = new Cursor(Cursor.DEFAULT_CURSOR);
		setCursor(defaultCursor);
	}
	
	/**
	 * Sets the cursor to a hand image
	 */
	public void setCursorToHand(){
		Cursor handCursor = new Cursor(Cursor.HAND_CURSOR);
		setCursor(handCursor);
	}
}
