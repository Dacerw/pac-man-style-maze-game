
/**
 * Interface used to interact with the settings for this application.
 */
public interface Settings {

	/**
	 * Loads the settings from a file.
	 */
	public void loadSettings();
	
	/**
	 * Determines the width of the window.
	 * @return an integer containing the width.
	 */
	public int getResolutionWidth();
	
	/**
	 * Determines the height of the window.
	 * @return an integer containing the height.
	 */
	public int getResolutionHeight();
	
	/**
	 * Determines whether the window is to be fullscreen.
	 * @return a boolean value containing true if fullscreen, false otherwise.
	 */
	public boolean getIsFullscreen();

	/**
	 * Determines whether the score in the game will count down (demonstration purposes only).
	 * @return a boolean value containing true if countdown, false otherwise.
	 */
	public boolean getScoreCountdown();
	
	/**
	 * Determines the graphics quality of the game.
	 * @return a String containing "low", "medium", or "high" depending on the graphics quality.
	 */
	public String getGraphicsQuality();
	
	/**
	 * Sets the width of the window.
	 * @param width an integer containing the width.
	 */
	public void setResolutionWidth(int width);
	
	/**
	 * Sets the height of the window.
	 * @param height an integer containing the height.
	 */
	public void setResolutionHeight(int height);
	
	/**
	 * Sets whether the window is to be fullscreen.
	 * @param isFullscreen a boolean containing true if fullscreen, false otherwise.
	 */
	public void setFullscreen(boolean isFullscreen);
	
	/**
	 * Sets whether the score is to countdown (demonstration purposes only).
	 * @param scoreCountdown a boolean containing true if countdown, false otherwise.
	 */
	public void setScoreCountdown(boolean scoreCountdown);
	
	/**
	 * Sets keyboard mappings for certain action, for a particular player.
	 * @param player a String representing the player : "Player1", "Player2", etc.
	 * @param up a String containing the mapping for moving up.
	 * @param down a String containing the mapping for moving down.
	 * @param left a String containing the mapping for moving left.
	 * @param right a String containing the mapping for moving right.
	 */
	public void setKeyboardSettings(String player, String up, String down, String left, String right);
	
	/**
	 * Sets the graphics quality.
	 * @param quality a String containing "low", "medium", or "high" depending on the graphics quality.
	 */
	public void setGraphicsQuality(String quality);

	/**
	 * Write the stored settings to a file.
	 */
	public void writeSettings();
}
