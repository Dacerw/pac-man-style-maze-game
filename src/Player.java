/**
 * Implementation of a Player in the maze.
 */
public class Player extends Entity {
	private int score;
	private int lives;
	
	public static final double PLAYER_RATIO_X = 0.52; //these ratio were measured based on the size of the graphics
	public static final double PLAYER_RATIO_Y = 0.64;
	
	public static final double PLAYER_SPEED = 400;
	
	private static final int ENTITY_TYPE = 0;
	
	public Player(int playerNum, int setScore, int setLives) {
		super(getSetX(playerNum), getSetY(playerNum), PLAYER_SPEED, playerNum);
		score = setScore;
		lives = setLives;
	}
	
	/**
	 * Increases or decreases the players score depending on where option is true or false.
	 * @param option, true value increases score, false value decreases
	 * @param change, is the value that score will be changed by
	 */
	public void changeScore(boolean option, int change) {
		if (option) {
			score = score + change;
		} else {
			score = score - change;
		}
	}
	
	/**
	 * Increments or decrements score
	 * @param option, true value increases score, false value decreases
	 */
	public void incDecScore(boolean option) {
		if (option) {
			score++;
		} else {
			score--;
		}
	}
	
	/**
	 * Increments or decrements lives
	 * @param option, true value increases lives, false value decreases
	 */
	public void incDecLives(boolean option) {
		if (option) {
			lives++;
		} else {
			lives--;
		}
	}
	
	/**
	 * Returns current score
	 * @return
	 */
	public int getScore() {
		return score;
	}
	
	/**
	 * Returns current lives
	 * @return
	 */
	public int getLives() {
		return lives;
	}
	
	/**
	 * Returns the size of the player in the x-axis as a fraction of the tile size (which is 1). 
	 */
	public double getSizeRatioX() {
		return PLAYER_RATIO_X;
	}
	
	/**
	 * Returns the size of the player in the y-axis as a fraction of the tile size (which is 1). 
	 */
	public double getSizeRatioY() {
		return PLAYER_RATIO_Y;
	}
	
	/**
	 * Returns the starting x position in order to offset the second player in multiplayer mode
	 * @param playerNum, the player identity
	 * @return, the starting x position
	 */
	private static double getSetX(int playerNum) {
		if (playerNum == 0) return Entity.TILE_SIZE*2/5;
		else return Entity.TILE_SIZE*3/5;
	}
	
	/**
	 * Returns the starting y position in order to offset the second player in multiplayer mode
	 * @param playerNum, the player
	 * @return, the starting y position
	 */
	private static double getSetY(int playerNum) {
		return Entity.TILE_SIZE/2;
	}
}